# coding: utf-8
import langid

__author__ = 'maria'
__descr__ = 'https://github.com/saffsd/langid.py'

#################################--VERY SIMPLE LANGUAGE CLASSIFICATION--################################################
def classify_language_of_text(self, text):
        return langid.classify(text)

#test
print(langid.classify("Καλημέρα!!!"))