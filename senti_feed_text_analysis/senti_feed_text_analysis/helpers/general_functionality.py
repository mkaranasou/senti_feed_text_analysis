import string

__author__ = 'maria'

##############################################clean text##############################################
def remove_non_printable_characters(text):
    clean_list = []
    assert isinstance(text, string);
    return filter(lambda x: x in string.printable, str(text))
    assert isinstance(text, list);
    for t in text:
        clean_list.append(filter(lambda x: x in string.printable, str(t)))
    return clean_list


#print remove_non_printable_characters("this is  \t")
print remove_non_printable_characters(["this", "is", "\t"])
#######################################################################################################

