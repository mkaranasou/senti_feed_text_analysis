__author__ = 'M'
import traceback
#import easy to use xml parser called minidom:
from xml.dom.minidom import parseString
#all these imports are standard on most modern python implementations

#open the xml file for reading:
file = open('../data/reviews/apparel_reviews/negative.review','r')
pfile = open('../data/reviews/apparel_reviews/negative_proccessed.review','a')

'''#convert to string:
data = file.read()
#close file because we dont need it anymore:
file.close()
#parse the xml you got from the file
try:
    dom = parseString(data)
    #retrieve the first xml tag (<tag>data</tag>) that the parser finds with name tagName:
    xmlTag = dom.getElementsByTagName('review_text')[0].toxml()
    #strip off the tag (<tag>data</tag>  --->   data):
    xmlData=xmlTag.replace('<review_text>','').replace('</review_text>','')
    #print out the xml tag and data in this format: <tag>data</tag>
    print xmlTag
    #just print the data
    print xmlData
except:
    traceback.print_exc()'''
def process_and_clean_review_files(file_to_read_from, file_to_append_to, mode='mac'):
    get_line = 0
    for line in file_to_read_from.readlines():
        if get_line==True:
            file_to_append_to.write(line)
        if mode == 'mac':
            if line =='<review_text>\n':
                get_line = True
        elif mode == 'win':
            if line =='<review_text>\r\n':
                get_line = True
        else:
            get_line = False

    file_to_read_from.close()
    file_to_append_to.close()