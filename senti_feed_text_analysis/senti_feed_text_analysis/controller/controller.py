import string
import traceback

from senti_feed_text_analysis.helpers.file_handler import prepare_line
import senti_feed_text_analysis.helpers.globals as g
import senti_feed_text_analysis.helpers.pyenchant_spell_checker as sp_ch


__author__ = 'maria'

sw_list = []
swn_list = []
rows = []
i = 0
o_line = prepare_line()
spell_checker = sp_ch.EnchantSpellChecker()

#connect to database and retrieve tweets, sentiwordnet eval, stopwords, top 20 emoticons and evaluation by hand
def get_required_data_from_db(mysql_server_conn):
    global rows, swn, stop_words_sample, emoticons
    rows = mysql_server_conn.execute_query(g.select_from_twitter_corpus.format(limit=10))
    swn = mysql_server_conn.execute_query(g.select_from_swn)
    stop_words_sample = mysql_server_conn.execute_query(g.select_from_stop_words)
    emoticons = mysql_server_conn.execute_query(g.select_from_top_emoticons)
    return rows


def define_handcoded_emotion_score(handcoded):
    handcoded_score = 1.0
    if handcoded == 'neutral' or handcoded == 'irrelevant':
        handcoded_score = 1.0
    elif handcoded == 'positive':
        handcoded_score = 2.0
    elif handcoded == 'negative':
        handcoded_score = 0.0
    return handcoded_score

#store results in db
def update_db_with_score(mysql_server_conn, id, score, words, emoticons, tweet_score, combined_score, emoticons_score, nvar_score,
                         non_nvar_score, original_tweet, lang, nvar_combi, aspect, references, hashlist,
                         new_hash_tag_list, handcoded):
    new_id = filter(lambda x: x in string.printable, str(id))
    final_tweet = prepare_query(words, "text")
    final_emoticons = prepare_query(emoticons, "list")

    handcoded_score = define_handcoded_emotion_score(handcoded)

    try:
        mysql_server_conn.update(g.update_twitter_corpus_q.format(str(score), str(final_tweet), str(final_emoticons),
                                tweet_score, combined_score, emoticons_score, nvar_score,
                                nvar_combi, str(new_id).strip('\n')))
        print "UPDATED:::", final_tweet
        '''mysql_server_conn.update(
            g.neg_book_review_insert.format(str(original_tweet).replace('\"', ''), final_tweet, score, tweet_score,
                                            nvar_score, nvar_combi))'''
    except:
        traceback.print_exc()
        #print g.update_results_query.format(id,original_tweet, final_tweet, handcoded, handcoded_score, tweet_score, emoticons_score, combined_score, score, nvar_score,
        #            non_nvar_score, nvar_combi, lang, emoticons, "", aspect, references, hashlist, str(new_hash_tag_list).replace("\"", " "))
        #try:#try to update already processed tweets -- if we cannot find them, try to insert results
        #    mysql_server_conn.update(g.insert_results_query.format(id,original_tweet, final_tweet, handcoded, handcoded_score, tweet_score, emoticons_score, combined_score, score, nvar_score,
        #            non_nvar_score, nvar_combi, lang, emoticons, "", aspect, references, hashlist, str(new_hash_tag_list).replace("\"", " ") ))
        #except:
        #    mysql_server_conn.update(g.update_results_query.format(id,original_tweet, final_tweet, handcoded, handcoded_score, tweet_score, emoticons_score, combined_score, score, nvar_score,
        #            non_nvar_score, nvar_combi, lang, emoticons, "", aspect, references, hashlist, str(new_hash_tag_list).replace("\"", " ") ))


def prepare_query(text_to_clean, mode):
    final_text = ""
    if mode == 'list':
        for text in text_to_clean:
            for i in range(0, text.__len__()):
                final_text = str(text[i]) + " "
    elif mode == 'text':
        for i in text_to_clean:
            final_text += str(i) + " "
    return final_text


#clean fetched senti word net data from db
#TODO REFACTORING
def clean_data_from_db():
    for row in swn:
        type = filter(lambda x: x in string.printable, str(row[0]))
        synset = filter(lambda x: x in string.printable, str(row[1]))
        swn_list.append((type, synset, row[2]))
        #clean stop words list data fetched from db
    for sw in stop_words_sample:
        test = (str(sw[0]).replace('\r', '').replace(',', ''))
        stop_word = filter(lambda x: x in string.printable, test)
        sw_list.append(stop_word)


#TODO pretty clumsy way of exporting results to file -- need to fix this!
def print_results(tweet_text, handcoded, score_text, like_score, navr_score, emoticons_score, combined, aspects):
    '''sys.stdout = f
    print o_line
    print "TWEET:\t", tweet_text
    print "HANDCODED_EMOTION:\t", handcoded
    print "SUM_SCORE:\t", score_text, like_score
    print "EMOTICONS_SCORE:\t", emoticons_score
    print "COMBINED_SUM_EMOTICONS:\t", combined
    print "NVAR_SCORE:\t", navr_score
    print "ASPECTS:\t", aspects'''
    f = open('../data/reviews/pos_book_results.txt', 'a')
    result = o_line + '\n' + "TWEET:\t" + tweet_text + '\n' + "HANDCODED_EMOTION:\t" + handcoded + '\n' + "SUM_SCORE:\t" + str(
        score_text) + "\t" + str(like_score) + '\n' + "EMOTICONS_SCORE:\t" + str(
        emoticons_score) + '\n' + "COMBINED_SUM_EMOTICONS:\t" + str(combined) + '\n' + "NVAR_SCORE:\t" + str(
        navr_score) + '\n' + "ASPECTS:\t" + str(aspects)
    f.write(result)
    #results = open('../data/results.txt', 'w')
    f.close()


#TODO REFACTORING
def start():
    global i
    for row in rows:
        i += 1
        try:
            #new TweetHandler object for every row/tweet
            #test_tweet = TweetHandler(row[0], row[1], None, swn_list, sw_list, mysql_server_conn, emoticons, spell_checker)
            #test_tweet = TweetHandler(i, row, swn_list, sw_list, mysql_server_conn, emoticons, spell_checker)

            #this is the text evaluation of tweet
            #score_text = (test_tweet.tweet_processor())

            '''update_db_with_score(row[0], score_text, test_tweet.words, test_tweet.smileys_per_sentence, test_tweet.tweet_score,
                                     test_tweet.combined_score, test_tweet.emoticons_score,test_tweet.nvar_score, test_tweet.non_nvar_score,
                                     row[1], test_tweet.language[0], test_tweet.nvar_combi_score, test_tweet.aspect,
                                     test_tweet.reference, test_tweet.hash_list, test_tweet.new_hashtag_list, row[2])'''
            '''update_db_with_score(i, score_text, test_tweet.words, test_tweet.smileys_per_sentence,
                                 test_tweet.tweet_score,
                                 test_tweet.combined_score, test_tweet.emoticons_score, test_tweet.nvar_score,
                                 test_tweet.non_nvar_score,
                                 row, test_tweet.language[0], test_tweet.nvar_combi_score, test_tweet.aspect,
                                 test_tweet.reference, test_tweet.hash_list, test_tweet.new_hashtag_list, 'neg_book')'''
            #print_results(test_tweet.tweet_text, row[2], score_text, test_tweet.tweet_score, test_tweet.nvar_score,
            #              test_tweet.emoticons_score, test_tweet.combined_score, test_tweet.aspect)
            #print_results(test_tweet.tweet_text, 'pos_book', score_text, test_tweet.tweet_score, test_tweet.nvar_score,
            #              test_tweet.emoticons_score, test_tweet.combined_score, test_tweet.aspect)

        except SyntaxError:
            print 'Syntax Error:', SyntaxError.message
            traceback.print_exc()
        except TypeError:
            print "Type Error:", TypeError.message
            traceback.print_exc()
        except:
            print "Undefined Exception:" + str(Exception.message)
            traceback.print_exc()

##################################### MAIN PROCESS #####################################################################
#print(datetime.datetime.now())
#get connection to db
#mysql_server_conn = database.MySQLDatabaseConnector()
#gather required for analysis data, e.g. twitter corpus, stopwords etc
#get_required_data_from_db(mysql_server_conn)
#fix weird problem with non-printing characters
#clean_data_from_db()
#fill N, V, A, R lists
#handle_files()
#prepare_enum_lists()
#get things rolling!
#start()
#finally close db connection
#mysql_server_conn.close_conn()
#print(datetime.datetime.now())

#print "OK:", i
########################################################################################################################__author__ = 'maria'
