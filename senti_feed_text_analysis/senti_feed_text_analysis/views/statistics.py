import matplotlib.pyplot as mpl
import database
import csv
import helpers.globals as g

__author__ = 'maria'

mysql_server_conn = database.MySQLDatabaseConnector()

def retrieve_data():
    global rows, results
    rows = mysql_server_conn.execute_query("SELECT id, TweetText, HandcodedEmotion,TextEmotionEval, EmoticonEmotionEval, "
                                           "CombinedScore, Emoticons, TextEmotion, ProcessedTweet, NVARScore FROM SentiFeed.TwitterCorpus WHERE NVARScore IS NOT NULL limit 2000")
    results = mysql_server_conn.execute_query(g.select_from_sentifeedresults)

def process_data():
    id =[]
    tweet_text = []
    handcoded_em = []
    text_score = []
    emoticon_score = []
    combined_score = []
    nvar_score = []
    results = []
    results_combi = []
    results_nvar = []

    counter = 0.0
    for row in rows:
        counter+= 1.0
        id.append(float(counter))
        tweet_text.append(row[1])
        if(row[2]=='negative'):
            handcoded_em.append(0.50)
            if row[3]<0.99:
                results.append("NEGATIVE")
            if row[5]<0.99:
                results_combi.append("NEGATIVE")
            if row[9]<0.99:
                results_nvar.append("NEGATIVE")
        elif(row[2]=='positive'):
            handcoded_em.append(1.50)
            if row[3]>1.05:
                results.append("POSITIVE")
            if row[5]>1.05:
                results_combi.append("POSITIVE")
            if row[9]>1.05:
                results_nvar.append("POSITIVE")
        elif(row[2]=='irrelevant'):
            handcoded_em.append(1.0)
            if row[3]<=1.05 and row[3]>=0.99:
                results.append("IRRELEVANT")
            if row[5]<=1.05 and row[5]>=0.99:
                results_combi.append("IRRELEVANT")
            if row[9]<=1.05 and row[9]>=0.99 :
                results_nvar.append("IRRELEVANT")
        elif(row[2]=='neutral'):
            handcoded_em.append(1.0)
            if row[3]<=1.05 and row[3]>=0.99:
                results.append("NEUTRAL")
            if row[5]<=1.05 and row[5]>=0.99:
                results_combi.append("NEUTRAL")
            if row[9]<=1.05 and row[5]>=0.99:
                results_nvar.append("NEUTRAL")
        text_score.append(float(row[3]))
        emoticon_score.append(float(row[4]))
        combined_score.append(float(row[5]))
        nvar_score.append(float(row[9]))
    print id
    print tweet_text
    print handcoded_em
    print text_score
    print emoticon_score
    print combined_score

    print results
    print results.__len__()

    print results_combi
    print results_combi.__len__()

    print results_nvar
    print results_nvar.__len__()

    '''ofile  = open('results.csv', "wb")
    writer = csv.writer(ofile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    for row in rows:
        writer.writerow(row)'''

    fig = mpl.figure()
    ax1 = fig.add_subplot(4,1,1, axisbg='grey')
    ax1.plot(id, handcoded_em, 'c', linewidth = 2)
    ax1.set_title('Handcoded Tweet Emotion Results')

    ax2 = fig.add_subplot(4,1,2, axisbg='grey')
    ax2.plot(id, text_score, 'c', linewidth = 2)
    ax2.set_title('Sum of Like Score/ occurrence')

    ax3 = fig.add_subplot(4,1,3, axisbg='grey')
    ax3.plot(id, combined_score, 'c', linewidth = 2)
    ax3.set_title('Combined Score')

    ax4 = fig.add_subplot(4,1,4, axisbg='grey')
    ax4.plot(id, nvar_score, 'c', linewidth = 2)
    ax4.set_title('N A V R Results')

    #mpl.axes().set_aspect('equal', 'datalim')
    mpl.show()

    mpl.plot(id, handcoded_em)
    #mpl.plot(id, text_score)
    mpl.plot(id, combined_score)
    mpl.plot(id, nvar_score)
    mpl.show()


retrieve_data()
process_data()
mysql_server_conn.close_conn()