from collections import OrderedDict
import traceback

from senti_feed_text_analysis.models.models import Score
from senti_feed_text_analysis.helpers import globals as g
import senti_feed_text_analysis.helpers.file_handler as fh
from senti_feed_text_analysis.helpers.globals import NOUNS, VERBS, ADJECTIVES, ADVERBS

__author__ = 'maria'


class FigurativeSentimentAnalyser(object):
    """
    Processes a text and calculates score::: negative (<1.0|| neutral (1.0)|| positive (>1.0)
    """
    def __init__(self, tweet, mysql_server_conn, calculation_methods_list, pos_tagger, top20emoticons, ta, tc):
        self.tweet = tweet
        self.mysql_server_conn = mysql_server_conn
        self.calculation_methods_list = calculation_methods_list
        self.pos_tagger = pos_tagger
        self.top20emoticons = top20emoticons
        self.ta = ta
        self.tc = tc
        self.scores_list = []
        ############ Scores ###############
        self.sentence_score = []
        self.tweet_score = 0.0
        self.sentence_emoticon_score = []
        self.tweet_emoticon_score = []
        self.emoticons_score = 0.0
        self.combined_score = 0.0
        self.sentiment_eval = ''
        self.nvar_score = 1.0
        self.non_nvar_score = 1.0
        self.nvar_combi_score = 1.0
        self.nvar_text_score = ""
        self.position_word_postag_score = []
        self.aspect = []
        self.aspect_g = []
        self.em = []
        self.nouns = []
        self.nouns_g =[]
        self.verbs = []
        self.verbs_g = []
        self.adjectives = []
        self.adjectives_g = []
        self.adverbs = []
        self.adverbs_g = []
        self.non_nvar = []
        self.non_nvar_g = []
        self.new_hashtag_list = []
        self.num_of_spell_checks_performed = 0
        ############ Scores ###############
        self.final_tweet = ""
        self.dict_vectorizer_data = []

    def process_methods(self):
        for m in self.calculation_methods_list:
            #print m.name, g.METHOD_TYPE.reverse_mapping[m.type], m.descr
            if m.type == g.METHOD_TYPE.GENERAL:
                self.scores_list.append(self.perform_general_sentiment_analysis(m))
            elif m.type == g.METHOD_TYPE.ASPECT_BASED:
                #self.scores_list.append(self.perform_aspect_sentiment_analysis(m))
                self.scores_list.append(self.perform_aspect_sentiment_analysis(m))
            elif m.type == g.METHOD_TYPE.GEO_BASED:
                #self.scores_list.append(self.perform_geo_sentiment_analysis(m))
                self.perform_geo_sentiment_analysis(m)
            else:
                raise NotImplemented

    def init_score(self, m_id):
        """
        Instantiate a Score object to hold method scores for the given tweet
        :param m_id: method id
        :type m_id: int
        :return: Score object
        :rtype: Score
        """
        score = Score(self.tweet.id, m_id)
        # emoticons score is precalculated and is the same for every method
        score.emoticons_score = self.emoticons_score
        return score

    def perform_general_sentiment_analysis(self, m):
        """
        Create a Score object and go on calculating text score, emoticons score, combined score
        """
        g.logger.info(":::General sentiment analysis for:::\t{0}".format(m.name))
        #init Score object
        score = self.init_score(m.id)

        if m.name == 'SUM':
            score.score = self.calculate_text_score()
            score.combined_score = self.calculate_combined_score(score.score)
        elif m.name == 'NVAR':
            self.get_n_v_a_r_from_pos_tag(self.pos_tagger.words_pos_tags)  # todo: make this return the lists
                                                                # we'll be using in next statement
            score.score = self.calculate_n_v_a_r_score(self.nouns, self.verbs, self.adjectives, self.adverbs)
            score.combined_score = self.calculate_combined_score(score.score)
        elif m.name == 'NVARCombi':
            score.score = self.calculate_non_nvar_score()
            score.combined_score = self.calculate_combined_score(score.score)
        elif m.name == 'COUNT':
            pass
        elif m.name == 'SUM & PUNCTUATION':
            score.score = self.calculate_text_score()
            score.combined_score = self.calculate_combined_score(score.score)
            self.process_non_word_characters(score.combined_score)

        elif m.name == 'NVAR & GATE_POS':
            self.get_n_v_a_r_from_pos_tag(self.pos_tagger.gate_tagger)
            score.score = self.calculate_n_v_a_r_score(self.nouns_g, self.verbs_g, self.adjectives_g, self.adverbs_g)
            score.combined_score = self.calculate_combined_score(score.score)
        elif m.name == 'DICT_VECTORIZER':
            g.logger.debug("DICT_VECTORIZER")
            dict_final_data = dict(self.tweet.pos_tagged_text.items()+self.tweet.tags.items())
            self.dict_vectorizer_data.append(dict_final_data)
            g.logger.debug("DICT_VECTORIZER self.dict_final_data %s "% dict_final_data)
            g.mysql_conn.update(g.update_figurative.format(tagged=self.tweet.tagged_text,
                                                           cleaned=self.tweet.cleaned_text,
                                                           tags=dict_final_data, id=self.tweet.id))
        else:
            pass
            #raise NotImplementedError

        score.processed_tweet = self.final_tweet

        g.logger.info("Score for {0}\t{1}:::\t{2}".format(score.processed_tweet, m.name, score.score))

        return score

    def perform_aspect_sentiment_analysis(self, m):
        """

        :param m:
        :return: :rtype: :raise NotImplemented:
        """
        #init Score object
        score = self.init_score(m.id)

        g.logger.info(":::General sentiment analysis for:::\t{0}".format(m.name))
        print m.name, m.type

        if m.name == 'ASPECT':
            try:
                score.score, score.aspects_list, score.aspects_score_list = \
                    self.calculate_aspect_score(self.pos_tagger.trigram_tagger[0], 'nvar')
            except IndexError:
                g.logger.error("No trigram tagger results")
                score.score = 1.0
                pass
            score.combined_score = self.calculate_combined_score(score.score)

        elif m.name == 'ASPECT & GATE_POS & NVAR':
            print 'ASPECT & GATE_POS & NVAR:::'
            score.score, score.aspects_list, score.aspects_score_list = \
                self.calculate_aspect_score(self.pos_tagger.gate_tagger, 'nvar')
            score.combined_score = self.calculate_combined_score(score.score)
        elif m.name == 'ASPECT & GATE_POS & SUM':
            score.score, score.aspects_list, score.aspects_score_list = \
            self.calculate_aspect_score(self.pos_tagger.gate_tagger, 'sum')
            score.combined_score = self.calculate_combined_score(score.score)
        else:
            raise NotImplemented

        score.processed_tweet = self.final_tweet

        g.logger.info("Score for {0}\t{1}:::\t{2}".format(score.processed_tweet, m.name, score.score))
        return score

    def perform_geo_sentiment_analysis(self, m):
        if m.name == '':
            pass
        elif m.name == '':
            pass
        else:
            raise NotImplemented

    ############## general preparation #############################################################
    def get_n_v_a_r_from_pos_tag(self, pos_tagger):
        """
        store nouns, verbs, adjectives, adverbs from pos tag in lists in order to check for score in respective files
        :param pos_tagger: pos tagging results from different taggers
        :return:
        """
        '''self.nouns =[]
        self.verbs = []
        self.adjectives = []
        self.adverbs = []
        self.aspect = []'''
        if type(pos_tagger) == list:  # in the case of gate tagger
            for word, tag in pos_tagger:
                if tag in NOUNS:
                    self.nouns_g.append(word)
                    self.aspect_g.append(word)  # moved from get_aspect to simplify
                elif tag in VERBS:
                    self.verbs_g.append(word)
                elif tag in ADJECTIVES:
                    self.adjectives_g.append(word)
                elif tag in ADVERBS:
                    self.adverbs_g.append(word)
                else:
                    self.non_nvar_g.append(word)

        else:
            for key in pos_tagger.keys():
                if pos_tagger[key] in NOUNS:
                    self.nouns.append(key)
                    self.aspect.append(key)  # moved from get_aspect to simplify
                elif pos_tagger[key] in VERBS:
                    self.verbs.append(key)
                elif pos_tagger[key] in ADJECTIVES:
                    self.adjectives.append(key)
                elif pos_tagger[key] in ADVERBS:
                    self.adverbs.append(key)
                else:
                    self.non_nvar.append(key)

    def calculate_n_v_a_r_score(self, nouns, verbs, adjectives, adverbs):
        """supposing that pos tagging works well
           we calculate the score from each word in lists
        :param nouns:
        """
        trials = 0
        misses = 0
        hits = 0
        missed = []

        #enumerate the lists in order to be able to get the position when score is found
        enumerated_lists = fh.n_v_a_r_lists
        #n_v_a_r_lists = self.prepare_nvar_lists()
        n_v_a_r_lists = [nouns, verbs, adjectives, adverbs]
        score_list = []
        score = 0.0
        #for the four lists noun, verb, adjective, adverb
        for nvar_list in enumerated_lists:
            for item in n_v_a_r_lists[enumerated_lists.index(nvar_list)]:
                trials += 1
                exists = self.find_if_and_where_in_list(nvar_list, item, 2)
                if exists != None and exists != []:
                    hits += 1
                    score_list.append(nvar_list[exists[0]][1][0])
                else:
                    misses += 1
                    missed.append(item)
        self.print_statistics_of_trials(hits, missed, misses, trials)
        if score_list.__len__() > 0:
            for i in score_list:
                score += float(i)
            self.nvar_score = score / score_list.__len__()
        else:
            #print "no NVAR score"
            self.nvar_score = 1.0
        return self.nvar_score

    def calculate_non_nvar_score(self):
        non_nvar_temp_score = 0.0
        score = self.calculate_non_nvar_sum_score(self.non_nvar)
        if score.__len__() > 0:
            for s in score:
                if not s is None:
                    non_nvar_temp_score += s
            if non_nvar_temp_score != 0.0:  # We do have Score!
                self.non_nvar_score = non_nvar_temp_score / score.__len__()

        return self.non_nvar_score

    def search_db_for_sum_score(self, word, spellchecking=False):
        """
        :param word:
        :param spellchecking:
        :return: :rtype:
        """
        try:
            stemmed_word = self.pos_tagger.lancaster_stemmer.stem(word.lower())
            if spellchecking:
                #return self.mysql_server_conn.execute_query(
                #    g.sum_query.format(self.ta.spell_checking(stemmed_word)))
                #g.logger.debug("<<<<<<<<<<" + stemmed_word)
                #g.logger.debug(">>>>>>>>>>" + self.ta.spell_checking( self.pos_tagger.lancaster_stemmer.stem(word.lower())))
                return [predict(self.ta.spell_checking(stemmed_word))]
            else:
                #return self.mysql_server_conn.execute_query(g.sum_query.format(stemmed_word))
                #return self.mysql_server_conn.execute_query(g.sum_query_figurative_scale.format(stemmed_word))
                return [predict(stemmed_word)]
        except:
            g.logger.error("search_db_for_sum_score SQL error:" + g.sum_query_figurative_scale.format(stemmed_word))
            #g.logger.error("search_db_for_sum_score SQL error:" + g.sum_query.format(stemmed_word))
            traceback.print_exc()
            return [[None],1]

    def calculate_non_nvar_sum_score(self, text):
        """
        :param text:
        :return: :rtype:
        """
        hits = 0
        misses = 0
        trials = 0
        missed = []
        score = []

        for word in text:
            if word.__len__() > 2:
                try:
                    trials += 1
                    temp_score = self.search_db_for_sum_score(word, False)
                    if not temp_score[0][0] == None:
                        hits += 1
                        score_per_times = temp_score[0][0] / temp_score[0][1]
                        score.append(score_per_times)
                    #if no score could be found for word, try spell checking -- duplicate code todo: refactor
                    elif self.ta.contains_errors:
                        temp_score = self.search_db_for_sum_score(word, True)
                        if not temp_score[0][0] == None:
                            score_per_times = temp_score[0][0] / temp_score[0][1]
                            score.append(score_per_times)
                    else:
                        misses += 1
                        missed.append(word)
                except:
                    traceback.print_exc()
        self.print_statistics_of_trials(hits, missed, misses, trials)
        return score

    def calculate_text_score(self):
        score = []
        times_found = 0.0
        final_score = 0.0
        temp_score = None
        hits = 0
        misses = 0
        trials = 0
        missed = []

        for word in self.get_tweet_words_in_a_single_list():
            if word.__len__() > 2 and word not in self.tweet.negations:  # take into account only words with more than 2 letters
                trials += 1
                temp_score = self.search_db_for_sum_score(word, False)
                #print "W/out SPELLCHEKCKING:", temp_score
                if not None == temp_score[0][0]:
                    score_per_times = temp_score[0][0] / temp_score[0][1]
                    if word in self.tc.negating_terms:
                        score_per_times = self.calculate_negation_score(score_per_times)
                    score.append(score_per_times)
                    hits += 1
                #if no score could be found for word, try spell checking -- duplicate code todo: refactor
                elif self.ta.contains_errors and word in self.ta.errors_in_text:
                    try:
                        g.logger.debug("contains errors {0}, try with {1}".format(self.ta.contains_errors, word))
                        temp_score = self.search_db_for_sum_score(word, True)
                        print "WITH SPELLCHEKCKING:", temp_score
                        if not temp_score[0][0] == None:
                            score_per_times = temp_score[0][0] / temp_score[0][1]
                            # since we have a score for spellchecked, we replace wrong word with good one
                            self.replace_spellchecked_word(word, self.ta.spell_checking(word))
                            if word in self.tc.negating_terms:
                                score_per_times = self.calculate_negation_score(score_per_times)
                            score.append(score_per_times)
                        g.logger.debug("Spellchecked result = {0}".format(word))
                    except:
                        traceback.print_exc()
                        score.append(1.0)
                else:
                    misses += 1
                    missed.append(word)
        self.sentence_score = score

                #TODO Too complicated for no reason. Make it cleaner
        if self.sentence_score.__len__() > 0:
            for s in self.sentence_score:
                final_score += s
            self.tweet_score = (final_score if self.tc.found_negations==False else 1.0-final_score)\
                               / self.sentence_score.__len__()
        else:  # NO SCORE
            self.tweet_score = 1.0

        self.print_statistics_of_trials(hits, missed, misses, trials)

        return self.tweet_score*10 if 0.0<self.tweet_score<1.0 else self.tweet_score

    def calculate_sentiment(self, score):
        """ 0: negative, 1:neutral, 2:positive etc. returns the string of evaluation
        :param score:float
        :return:None
        """
        emotion = ""
        if self.tweet_score < 0 or self.tweet_score == 1.0:
            emotion = "neutral"
        elif 1.0 < self.tweet_score < 1.75:
            emotion = "somewhat positive"
        elif self.tweet_score >= 1.75:
            emotion = "positive"
        elif 1.0 > self.tweet_score > 0.25:
            emotion = "somewhat negative"
        elif 0.0 <= self.tweet_score <= 0.25:
            emotion = "negative"

        g.logger.info("Tweet/Emotion = {0}, {1}".format(self.tweet.text, emotion))
        return emotion

    def calculate_combined_score(self, score):
        """
        combined means text score and emoticons score
        combined score calculation = [(emoticons score/ num of emoticons) + general method score]/2.0
        :param score:
        :return:
        """
        if self.em.__len__() > 0:  # if there is emoticons score - go on and calculate combi score for any method
            self.combined_score = ((self.emoticons_score / self.em.__len__()) + score) / 2.0
            self.nvar_combi_score = ((self.emoticons_score / self.em.__len__())
                                     + self.nvar_score + self.non_nvar_score) / 3.0
            return ((self.emoticons_score / self.em.__len__()) + score) / 2.0
        else:
            self.combined_score = score
            self.nvar_combi_score = (self.nvar_score + self.non_nvar_score) / 2.0
            return score

    ######################################### ASPECT ANALYSIS ##########################################################
    def get_noun_score(self, tag, word):
        noun_exists = self.find_if_and_where_in_list(enumerate(fh.n_v_a_r_lists[g.WORD_TYPE.NOUN]), word, 'aspect')
        if noun_exists != []:
            noun_score = fh.n_v_a_r_lists[g.WORD_TYPE.NOUN][noun_exists[0]][1][0]
            g.logger.info("{0}:{1}\t--->{2}".format(word, tag, noun_score))
        else:
            noun_score = 1.0

        return noun_exists, noun_score

    def get_aspect_var_pair_score(self, candidate, word_type):
        """
        :param word_type:
        :param candidate:
        """
        exists = self.find_if_and_where_in_list(enumerate(fh.n_v_a_r_lists[word_type]), candidate, 'aspect')
        if None != exists and exists != []:
            #print fh.n_v_a_r_lists[word_type][exists[0]][0]
            #print fh.n_v_a_r_lists[word_type][exists[0]][1]

            g.logger.debug("get_aspect_var_pair_score:::{0}".format(fh.n_v_a_r_lists[word_type][exists[0]][1]))

            return fh.n_v_a_r_lists[word_type][exists[0]][1][0]
        else:
            return None

    def get_candidates_from_bigrams(self, word):
        """

        :param word:
        :return: :rtype:
        """
        candidates = []
        for word1, word2 in self.pos_tagger.bigram_tagger:
            # find pairs that noun is part of and keep the other word in order to get score afterwards
            if word1 == word and word2 not in self.nouns:
                candidates.append(word2)
            elif word2 == word and word1 not in self.nouns:
                candidates.append(word1)
        return candidates

    def get_candidate_score(self, candidates, noun_score, score_list, type):
        """
        # for each pair word found, try to get a score
        :param candidates:
        :param noun_score:
        :param score_list:
        """
        for candidate in candidates:
            tmp_score = []
            if candidate in self.verbs:
                g.logger.info("Verb candidate:::\t{0}\t{1}".format(candidate, self.verbs.index(candidate)))
                if type == 'nvar':
                    verb_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.VERB)
                    if verb_score != None and verb_score!=[]:
                        score_list.append(verb_score)
                        score_list.append(noun_score)
                        g.logger.info("Verb score:::\t{0}\t{1}".format(candidate, verb_score))
                    else:
                        pass

                elif type == 'sum':
                    tmp_score == self.search_db_for_sum_score(candidate, False)
                    if tmp_score!=[] and tmp_score!=None:
                        verb_score = tmp_score[0][0] / tmp_score[0][1]
                        score_list.append(verb_score)
                        score_list.append(noun_score)

            elif candidate in self.adjectives:
                g.logger.info("Adjective candidate:::\t{0}\t{1}".format(candidate, self.adjectives.index(candidate)))
                if type == 'nvar':
                    adj_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.ADJECTIVE)
                    if adj_score != None and adj_score!=[]:
                        score_list.append(adj_score)
                        score_list.append(noun_score)
                        g.logger.info("Adjective score:::\t{0}\t{1}".format(candidate, adj_score))

                    else:
                        # If adjective could not be found then try with adverb list
                        adv_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.ADVERB)
                        if adv_score is not None and adv_score!=[]:
                            score_list.append(adv_score)
                            score_list.append(noun_score)
                            g.logger.info("Adj/Adverb score:::\t{0}\t{1}".format(candidate, adv_score))
                elif type == 'sum':
                    tmp_score == self.search_db_for_sum_score(candidate, False)
                    if tmp_score!=[] and tmp_score!=None:
                        adj_score = tmp_score[0][0] / tmp_score[0][1]
                        score_list.append(adj_score)
                        score_list.append(noun_score)

            elif candidate in self.adverbs:
                g.logger.info("Adverb candidate:::\t{0}\t{1}".format(candidate, self.adverbs.index(candidate)))
                if type == 'nvar':
                    adv_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.ADVERB)
                    if adv_score != None and adv_score!=[]:
                        score_list.append(adv_score)
                        score_list.append(noun_score)
                        g.logger.info("Adverb score:::\t{0}\t{1}".format(candidate, adv_score))

                    else:
                        # try with adjective list if not in adverb list
                        adj_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.ADJECTIVE)
                        if adj_score != None and adv_score!=[]:
                            score_list.append(adj_score)
                            score_list.append(noun_score)
                            g.logger.info("Adv/Adjective score:::\t{0}\t{1}".format(candidate, adj_score))
                elif type == 'sum':
                    tmp_score == self.search_db_for_sum_score(candidate, False)
                    if tmp_score!=[] and tmp_score!=None:
                        adv_score = tmp_score[0][0] / tmp_score[0][1]
                        score_list.append(adv_score)
                        score_list.append(noun_score)

            else:
                g.logger.info("None fits candidate:::\t{0}".format(candidate))

    def calculate_aspect_score(self, pos_tagger, type):
        """
        :param pos_tagger:
        """
        final = 0.0
        all_aspect_scores = []
        temp_aspect_results = []
        tagger = pos_tagger
        aspects_list = []
        aspects_score_list = []

        print "ASPECT::::::", self.aspect

        try:
            for word, tag in tagger:
                final_aspect_score = 0.0
                score_list = []
                noun_list = []
                if tag in NOUNS and word not in noun_list:  # in order not to have multiple aspects
                    noun_list.append(word)
                    aspects_list.append(word)
                    if tag != 'NN':  # if we don't have a pure Noun, then try stemming
                        g.logger.debug("Before stemming noun:::{0}".format(word))
                        word = self.pos_tagger.lancaster_stemmer.stem(word)
                        g.logger.debug("After stemming noun:::{0}".format(word))

                    # if tagged word is a noun then go on and check bigrams.
                    candidates = self.get_candidates_from_bigrams(word)

                    # keep aspect and unique list of matching
                    temp_aspect_results.append([word, OrderedDict.fromkeys(candidates).keys()])

                    # firstly get the Noun's score --> usually 1.0 : neutral
                    if type == 'nvar':
                        noun_exists, noun_score = self.get_noun_score(tag, word)
                    elif type == 'sum':
                        noun_exists = self.search_db_for_sum_score(word, False)
                        #print "noun exists", noun_exists
                        if noun_exists!=[] and noun_exists[0][0]!=None:
                            noun_score = noun_exists[0][0]/noun_exists[0][1]
                        else:
                            noun_score = 1.0

                    # try to find for every candidate a score in corresponding V A R list and append it to score_list
                    self.get_candidate_score(candidates, noun_score, score_list, type)

                    for score in score_list:
                        final_aspect_score += float(score)
                    if score_list.__len__()>0:
                        score = final_aspect_score/float(score_list.__len__())
                    else:
                        score = noun_score

                    all_aspect_scores.append(score)
                    aspects_score_list.append(score)
                    g.logger.info("Score for aspect {0} {1} is {2}".format(noun_list[-1], candidates, score))

            g.logger.info("Aspect analysis:::\t"+str(temp_aspect_results))
            g.logger.info("Aspect - Score Lists:::\t{0} - {1}".format(aspects_list, aspects_score_list))

        except IndexError:
            g.logger.error("No trigram list.")
            traceback.print_exc()

        if all_aspect_scores.__len__() > 0:
            for s in all_aspect_scores:
                try:
                    final += float(s)
                except TypeError:
                    traceback.print_exc()
            g.logger.info("Score for aspects:::{0}".format(final/all_aspect_scores.__len__()))
            return final/all_aspect_scores.__len__(), aspects_list, aspects_score_list
        return 1.0, aspects_list, aspects_score_list

######################################### ASPECT ANALYSIS ##############################################################
######################################### EMOTICONS ####################################################################
    def get_emoticons_score(self):
        """
        Look up every emoticon found in smileys per sentence list. If found, save score in em
        """
        new_list = list(enumerate(self.top20emoticons))
        for each in self.tweet.smileys_per_sentence:
            for emoticon in each:
                exists = [n for n, (i, s) in new_list if i == emoticon]
                if exists != None and exists != []:
                    self.em.append(new_list[exists[0]][1][1])

    def calculate_emoticons_score(self):
        if self.em.__len__()>0:
            for s in self.em:
                self.emoticons_score += s
        else:
            self.emoticons_score = 1.0

######################################## GENERAL HELPERS ###############################################################
    def process_non_word_characters(self, score):
        """
        Many fullstops or exclamation marks could indicate intensity of emotion
        :param score:
        """
        for char in self.tweet.non_word_chars_removed:  # [[non word chars in sent1],[non word chars in sent2]...]
            exclamation = 0
            questionmark = 0
            fullstop = 0
            for c in char:  #sentence-wise approach
                if c == '!':  # exclamation mark should mean amplification of emotion
                    exclamation += 1
                elif c == '?':  # a question should probably be handled as neutral
                    questionmark += 1
                elif c == '.':  # a question should probably be handled as neutral
                    fullstop += 1
                else:
                    pass
            # if '!!!' should mean excitement
            if exclamation / 3 >= 1:
                self.amplify_emotion_by(0.1, score)
            # if '!' should mean small indication of excitement...?
            elif exclamation > 0:
                if questionmark > 0:  #this means sth like ?! that should indicate being puzzled
                    self.amplify_emotion_by(-0.05, score)
                else:
                    self.amplify_emotion_by(0.05, score)
            # if '?' should be more close to neutral...
            if questionmark > 0:
                self.amplify_emotion_by(0.0, score)
            #if '...' i believe emotion should be amplified somehow because it usually indicates sth... but to be figured out
            if fullstop / 3 >= 1:
                self.amplify_emotion_by(0.1, score)
            g.logger.debug("exclamation: {0}, fullstop: {1}, quetionmark: {2}".format(exclamation, fullstop, questionmark))

    def amplify_emotion_by(self, amp_by, score):  # noooot that smart :P mpliaxmpliaxmpliax!!! todo: fix it!!!
            """
            :param amp_by:
            :param score:
            """
            if 1.0 < score < 1.90:
                score = score + amp_by
                g.logger.debug("+{0}".format(amp_by))
            elif 1.0 > score > 0.10:
                score = score - amp_by
                g.logger.debug("-{0}".format(amp_by))
            else:
                g.logger.debug("We do not amplify a neutral score.?")

    def get_tweet_words_in_a_single_list(self):
        tweet_words = []
        try:
            if type(self.tweet.words[0]) == list:
                for each in self.tweet.words:
                    tweet_words += each
            else:
                tweet_words = self.tweet.words
        except IndexError:
            tweet_words = self.tweet.words

        return tweet_words

    #done
    def find_if_and_where_in_list(self, list_to_search, what_to_search_for, args):
        """
        to find if sth exists in an enumerated list [[1,[1st, 2nd]],[2,[1st, 2nd]]] when we care about where.
        """
        exists = None
        if args == 1:
            exists = [a for a, (i, s) in list_to_search if i == what_to_search_for]
        elif args == 2:
            exists = [a for a, (i, s) in list_to_search if s == what_to_search_for]
        elif args == 'aspect':
            exists = [a for a, (i, s) in list_to_search if what_to_search_for in s]

        return exists

    def print_statistics_of_trials(self, hits, missed, misses, trials):
        """
        Calculates for how many words score was found (hits), not found (misses), trials to get score (trials) and
        which words were missed (missed)
        :param hits:
        :param missed:
        :param misses:
        :param trials:
        """
        try:
            stats = "TRIALS:\t{0}\tHITS:\t{1}\tMISSES:\t{2}\tTOTAL WORDS:\t{3}\tPERCENTAGE:\t{4}%"\
            .format(trials, hits, misses, self.tweet.words.__len__(), (hits / float(self.tweet.words.__len__())) * 100)
            missed_words = "MISSED WORDS:"+ str(missed)

            g.logger.debug(stats + '\t' + missed_words)
        except ZeroDivisionError:
            g.logger.error("Division by zero error: Words length = {0} {1}".format(self.tweet.words.__len__(), self.tweet.words))
            pass

    def calculate_negation_score(self, word_score):
        g.logger.debug("Reversing score::: {0} ".format(word_score))
        if word_score > 1.0:
            return word_score - 1.0
        else:
            return word_score + 1.0

    def replace_spellchecked_word(self, word, spellchecked_word):
        #if self.tweet.sentences.__len__()>1 and self.tweet.sentences[1]!=[]:
        '''for each in self.tweet.words:
            if type(each)==list:
                 if word in each:
                    each[each.index(word)] = spellchecked_word
                    print "replace spellchecked1", each, word, spellchecked_word'''
        #else:
        if word in self.tweet.words:
                self.tweet.words[self.tweet.words.index(word)] = spellchecked_word
                print "replace spellchecked2", self.tweet.words, word, spellchecked_word
        else:
                print "problem"

def predict(word):
    xx = g.vectorizer.transform(word)
    predictions =  g.cls.predict(xx)
    print "PREDICTION FOR ", word, predictions
    if predictions.all =='negative':
        return [0.0, 1]
    elif predictions.all == 'positive':
        return [2.0, 1]
    else:
        return [1.0, 1]
    #return [predictions, 1]