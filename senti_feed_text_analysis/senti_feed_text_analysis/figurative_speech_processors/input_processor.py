from threading import Thread
import threading

from senti_feed_text_analysis.controller.sentifeed_main import SentiFeed
from senti_feed_text_analysis.helpers import globals as g
from senti_feed_text_analysis.stream.stream_handler import StreamHandler


__author__ = 'maria'
__descr__ = 'This class is responsible to initiate the correct process depending on source input'


class InputHandler(object):
    def __init__(self, source_type, args, evaluation=False, table = None):
        self.source_type = source_type
        self.args = args
        self.evaluation = evaluation
        self.sentifeed = SentiFeed(g.DB_TYPE.MYSQL, g.DATA_TYPE.TWITTER_CORPUS, g.DATA_TYPE.SWN,
                      g.DATA_TYPE.STOP_WORDS, g.DATA_TYPE.EMOTICONS, table, self.evaluation)
        self.sentifeed.get_things_rolling()
        self.thread_list = g.THREAD_LIST

    def process_corpus_only(self, corpus):
        """
        Start processing corpus
        :param corpus: User Choice of corpus
        :return:
        """
        self.sentifeed.start_processing(self.sentifeed.get_data_list_item(g.DATA_TYPE.TWITTER_CORPUS))
        if self.evaluation:
            self.sentifeed.evaluate_score()
            self.sentifeed.present_results()
        self.sentifeed.save_results()
        self.sentifeed.clean_up()

    def process_text_only(self, text):
        """
        For text processing, we do not need to evaluate against corpus and save to db
        :param text: User Input - string
        :return:
        """
        self.sentifeed.start_processing(text)
        if self.evaluation:
            self.sentifeed.evaluate_score()
            self.sentifeed.present_results()
        self.sentifeed.save_results()
        self.sentifeed.clean_up()

    def process_streaming_data(self, term=None, area=None, async=True):
        """
        Different approach is being followed with streaming data.
        :param async:
        :param area:
        :param term:
        :return:
        """
        stream_handler = StreamHandler()
        thread_handler_start = ThreadHandler(stream_handler, g.JOB.START, sleep_time=0.10,
                                       thread_name='StreamThread', args= (term, area, async) )
        self.thread_list[g.THREAD_LIST.__len__()-1].start()

        print "thread for {0}{1}{2} just started!".format(term, area, async)
        self.sentifeed.start_processing(g.SOURCE.Stream)

        if self.evaluation:
            self.sentifeed.evaluate_score()
            self.sentifeed.present_results()
        #self.sentifeed.save_results()
        stream_handler.stop_listening()
        thread_handler_start.stop_event.set()
        print thread_handler_start.stop_event.is_set()

        print "=================================stop thread ==========================="
        thread_handler_stop = ThreadHandler(stream_handler, g.JOB.STOP, sleep_time=0.10,
                               thread_name='StreamStopThread', args= (term, area, async))
        self.thread_list[g.THREAD_LIST.__len__()-1].start()
        thread_handler_stop.stop_event.set()
        print "=================================stop thread stopped ==========================="

        print "stopped threads {0} {1} {2}".format(g.THREAD_LIST.__len__(), g.THREAD_LIST[0].is_alive(), g.THREAD_LIST[1].is_alive())
        self.sentifeed.clean_up()

    def init_processing(self, text, term=None, area=[], async=False):
        """
        Depending on source type, begin processing.
        :param text: text to be evaluated
        :param term: term for search in case of stream
        :param area: area for search in case of stream
        :param async: how to get stream working
        :return:
        """
        if self.source_type == g.SOURCE.Corpus:
            self.process_corpus_only(self.args)
            #self.sentifeed.start_processing(self.sentifeed.get_data_list_item(g.DATA_TYPE.TWITTER_CORPUS))
        elif self.source_type == g.SOURCE.Text:
            self.process_text_only(text)
        elif self.source_type == g.SOURCE.Stream:
            self.process_streaming_data(term, area, async)
        else:
            raise NotImplementedError


class ThreadHandler(object):
    def __init__(self, stream_handler, function, thread_name, sleep_time, args=()):
        self.function = function
        self.stop_event = threading.Event()
        g.THREAD_LIST.append(Thread(target=self.do_things, name=thread_name,
                                    args=args + (stream_handler, sleep_time, self.stop_event)))

    def stop(self):
        self.stop_event.set()

    def do_things(self,term, area, async, stream_handler, sleep_time, stop_event):
        if self.function == g.JOB.START:
            print "inside do things ::: {0}".format('start_listening')
            while not stop_event.is_set():
                stream_handler.start_listening(term, area, async)
                if stop_event.is_set:
                    stream_handler.stop_listening()
                    break
        elif self.function == g.JOB.STOP:
            print "inside do things {0}".format('stop_listening')
            stream_handler.stop_listening()

        print "Checked event"
        print "Got out..."



############ TEST #############
input_handler = InputHandler(g.SOURCE.Corpus, None, False, table=g.TABLE.FigurativeTrainingData)
input_handler.init_processing(None, 'Apple')