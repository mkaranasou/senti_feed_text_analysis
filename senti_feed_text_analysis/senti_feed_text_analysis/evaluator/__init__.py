from senti_feed_text_analysis.helpers.scale_converter import ScaleConverter

__author__ = 'maria'

from enum import Enum
from senti_feed_text_analysis.helpers import globals as g
METHOD_TYPE = Enum('method_type', GENERAL=1, ASPECT_BASED=2)


class Evaluator(object):
    """
    method_list = [CalculationMethod1, CalculationMethod2,... n]
    depending on type of method:
    results_list = [[[score1, [aspects]],[score2, [aspects]]], [[score1, [aspects]],[score2, [aspects]]]]
    or results_list = [[score1, score2... n],[score1, score2... n]]
    corpus_data = [handcoded_score1, handcoded_score2..., n]

    results_list[0...method_list.__len__()].__len__() == corpus_data.__len__()

    the whole point is:
    if item in results_list[method_index] equals corpus_data[item_index]
        then accuracy is 100%
    else
        accuracy is 0.
    """
    def __init__(self, method_list, score_list, corpus_data_list, source, tweet_obj_list, table=g.TABLE.TwitterCorpus):
        self.method_list = method_list  # contains CalculationMethod objects
        self.score_list = score_list
        self.source_table = table
        self.scale_converter = ScaleConverter(old_max=5.0, old_min=-5.0, new_max=2.0, new_min=0.0)     # by default initialized to convert to (-5.0, 5.0)
        self.corpus_data_list = self.preproccess_corpus_result_list(corpus_data_list)  # [(id, text, score, aspect),]
        self.accuracy_result_list = [] * self.method_list.__len__()
        #self.score_accuracy_list = [[] * len(self.method_list)]
        self.score_accuracy_list = [list([]) for _ in range(0, self.method_list.__len__())]
        self.combined_accuracy_list = [list([]) for _ in range(0, self.method_list.__len__())]
        self.aspect_accuracy_list = [list([]) for _ in range(0, self.method_list.__len__())]
        self.aspect_score_accuracy_list = [list([]) for _ in range(0, self.method_list.__len__())]
        self.source = source
        self.tweet_obj_list = tweet_obj_list
        self.non_english_tweets = self.calculate_non_english_tweet_count()

    def calculate_accuracy_for_each_method(self):
        for m in self.method_list:
            if m.type == g.METHOD_TYPE.GENERAL:
                self.calculate_general_emotion_accuracy(m)
            elif m.type == g.METHOD_TYPE.ASPECT_BASED:
                self.calculate_aspect_based_accuracy(m)
            elif m.type == g.METHOD_TYPE.GEO_BASED:
                self.calculate_geo_based_accuracy(m)

    def calculate_general_emotion_accuracy(self, m):
        """
        for every method, iterate through results and if handcoded and results agree, then accuracy is 100%
        :param m: CalculationMethod object
        :return:Nothing
        """
        m_index = self.method_list.index(m)
        self.calculate_score_accuracy(m_index)
        self.calculate_combined_accuraccy(m_index)
        #self.calculate_aspect_accuracy(m_index)

    #todo: it should be result for aspect...
    def calculate_aspect_based_accuracy(self, m):
        #fixme: temporary just to know if it is working correctly
        m_index = self.method_list.index(m)
        self.calculate_score_accuracy(m_index)
        self.calculate_combined_accuraccy(m_index)
        self.calculate_aspect_accuracy(m_index)

    def calculate_score_accuracy(self, m_index):
        for scores in self.score_list:  # for each list of scores that concern one tweet
            g.logger.debug("General accuracy:::\tItem:{0}, Score:{1}, Corpus Score:{2}, Emoticons Score:{3}"
                           .format(self.score_list.index(scores), scores[m_index].score,
                                   self.corpus_data_list[self.score_list.index(scores)][0], 
                                   scores[m_index].emoticons_score))
            if self.tweet_obj_list[self.score_list.index(scores)] != '':  # if we have an english tweet then proceed
                if self.agree(scores[m_index].score, self.corpus_data_list[self.score_list.index(scores)][0]):
                    # if result and corpus result are on the same page then 100% accuracy
                    self.score_accuracy_list[m_index].append(100)
                else:
                    self.score_accuracy_list[m_index].append(0)

    def calculate_combined_accuraccy(self, m_index):
        for score in self.score_list:  # for each list of scores that concern one tweet
            g.logger.debug("Combined accuracy:\tItem:{0}, Combined Score:{1}, Corpus Score:{2}, Emoticons Score:{3}"
                           .format(self.score_list.index(score), score[m_index].combined_score,
                            self.corpus_data_list[self.score_list.index(score)][0], score[m_index].emoticons_score))
            if self.tweet_obj_list[self.score_list.index(score)] != '':  # if we have an english tweet then proceed
                if self.agree(score[m_index].combined_score, self.corpus_data_list[self.score_list.index(score)][0]):
                    # if result and corpus result are on the same page then 100% accuracy
                    self.combined_accuracy_list[m_index].append(100)
                else:
                    self.combined_accuracy_list[m_index].append(0)

    def calculate_aspect_accuracy(self, m_index):
        for score in self.score_list:
            aspect = self.corpus_data_list[self.score_list.index(score)][1]
            text_index = self.score_list.index(score)

            g.logger.debug("corpus_aspect/score.aspects_list:::\t{0}\t{1}".format(aspect, score[m_index].aspects_list))
            if self.tweet_obj_list[self.score_list.index(score)] != '':  # if we have an english tweet then proceed
                # if the aspect in corpus list matches one in our aspect list
                if aspect in score[m_index].aspects_list:
                    aspect_index = score[m_index].aspects_list.index(aspect)
                    self.aspect_accuracy_list[m_index].append(100)

                    # if we have a match in aspect check if we also have a match in aspect score
                    if self.agree(score[m_index].aspects_score_list[aspect_index], self.corpus_data_list[text_index][0]):
                        self.aspect_score_accuracy_list[m_index].append(100)
                    else:
                        self.aspect_score_accuracy_list[m_index].append(0)
                else:
                    # we have found no matches, so 0% for both
                    self.aspect_accuracy_list[m_index].append(0)
                    #self.aspect_score_accuracy_list[m_index].append(0)

    def agree(self, our_score, actual_score):
        # if self.source_table == g.TABLE.FigurativeTrainingData:
        #     # if what we predicted and the actual score differ by +-0.5
        #     scaled_score = self.scale_converter.convert(abs(our_score))
        #     g.logger.debug("scaled_score - actual_score = {0} - {1} = {2}".format(abs(scaled_score),
        #                                                                           actual_score,
        #                                                                           abs(scaled_score)-abs(actual_score)))
        #     if -0.5 <= (abs(scaled_score)-abs(actual_score)) <= 0.5:
        #         return True
        #     else:
        #         return False
        # else:
        #     '''scaled_score = self.scale_converter.convert(abs(actual_score))
        #     g.logger.debug("scaled_score - actual_score = {0} - {1} = {2}".format(abs(our_score), abs(scaled_score),
        #                                                                                abs(our_score)-abs(scaled_score)))'''
            if our_score > 1.0 and actual_score > 1.0 or our_score == 1.0 \
                    and actual_score == 1.0 or our_score < 1.0 and actual_score < 1.0:
                return True
            else:
                return False

    def calculate_final_accuracy_per_method(self):
        for i in range(0, self.method_list.__len__()):
            self.method_list[i].accuracy = self.calculate_accuracy(self.score_accuracy_list[i], self.non_english_tweets)
            self.method_list[i].combined_accuracy = self.calculate_accuracy(self.combined_accuracy_list[i], self.non_english_tweets)
            self.method_list[i].aspect_accuracy = self.calculate_accuracy(self.aspect_accuracy_list[i], 0)
            self.method_list[i].aspect_score_accuracy = self.calculate_accuracy(self.aspect_score_accuracy_list[i], 0)

    def calculate_accuracy(self, score_list, non_english_tweets):
        """
        Simply add all scores and divide by number of scores
        """
        accuracy = 0.0
        if score_list.__len__() > 0:
            for r in score_list:
                accuracy += r
            print "ACC:::", score_list.__len__(), accuracy, score_list, non_english_tweets
            return accuracy/float(score_list.__len__() - non_english_tweets)
        return 0.0

    def calculate_non_english_tweet_count(self):
        non_english_tweets = 0.0
        for each in self.tweet_obj_list:
            if each == '':
                non_english_tweets += 1.0
        return non_english_tweets


    def preproccess_corpus_result_list(self, corpus):
        proccessed_list = []
        for c in corpus:
                # if self.source_table != g.TABLE.FigurativeTrainingData:
                    if 'positive' in c.emotion_text:
                        proccessed_list.append([2.0, c.initial_aspect])
                    elif 'negative' in c.emotion_text:
                        proccessed_list.append([0.0, c.initial_aspect])
                    elif 'irrelevant' in c.emotion_text or 'neutral' in c.emotion_text:
                        proccessed_list.append([1.0, c.initial_aspect])
                # else:
                #     proccessed_list.append([c.emotion_num, c.initial_aspect])
        return proccessed_list

    def calculate_geo_based_accuracy(self, m):
        pass

    def get_methods_evaluated(self):
        self.calculate_accuracy_for_each_method()
        self.calculate_final_accuracy_per_method()
