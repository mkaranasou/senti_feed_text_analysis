import re
import traceback
from senti_feed_text_analysis.processors.hashtag_handler import HashtagHandler

__author__ = 'maria'
import senti_feed_text_analysis.helpers.globals as g

"""
Todo:
1. Tag All corpuses and use them to train svm - ok
2. Keep a data part for test
3. When classifying new tweets:
    Clean them and tag them.
4. add positive HT and negative HT
"""


class TextTagger(object):
    """
    TextTagger is responsible for applying identified tags to a cleaned text.
    The result should be:
    {REFERENCE} {LINK}
    """
    _link_pattern = g.LINK_PATTERN
    _pos_smiley_pattern = g.POS_SMILEY_PATTERN
    _neg_smiley_pattern = g.NEG_SMILEY_PATTERN
    _negation_pattern = g.NEGATIONS_PATTERN
    _reference_pattern = g.REFERENCE_PATTERN
    _ht_pattern = g.HT_PATTERN
    _rt_pattern = g.RT_PATTERN
    _laugh_pattern = g.LAUGH_PATTERN
    _love_pattern = g.LOVE_PATTERN
    _capital_pattern = g.CAPITALS_PATTERN
    _patterns_ = {
        g.TAGS.CAPITAL: _capital_pattern,
        g.TAGS.HT: _ht_pattern,
        g.TAGS.LINK: _link_pattern,
        g.TAGS.POS_SMILEY: _pos_smiley_pattern,
        g.TAGS.NEG_SMILEY: _neg_smiley_pattern,
        g.TAGS.NEGATION: _negation_pattern,
        g.TAGS.REFERENCE: _reference_pattern,
        g.TAGS.RT: _rt_pattern,
        g.TAGS.LAUGH: _laugh_pattern,
        g.TAGS.LOVE: _love_pattern,
    }
    _tags = {
        g.TAGS.reverse_mapping[g.TAGS.CAPITAL]: "no",
        g.TAGS.reverse_mapping[g.TAGS.LINK]: "no",
        g.TAGS.reverse_mapping[g.TAGS.POS_SMILEY]: "no",
        g.TAGS.reverse_mapping[g.TAGS.NEG_SMILEY]: "no",
        g.TAGS.reverse_mapping[g.TAGS.NEGATION]: "no",
        g.TAGS.reverse_mapping[g.TAGS.REFERENCE]: "no",
        g.TAGS.reverse_mapping[g.TAGS.HT]: "no",
        g.TAGS.reverse_mapping[g.TAGS.HT_POS]: "no",
        g.TAGS.reverse_mapping[g.TAGS.HT_NEG]: "no",
        g.TAGS.reverse_mapping[g.TAGS.RT]: "no",
        g.TAGS.reverse_mapping[g.TAGS.LAUGH]: "no",
        g.TAGS.reverse_mapping[g.TAGS.LOVE]: "no"
    }

    def __init__(self):
        self.tagged_text = ''
        self.tag_pattern = " {%s} "
        self.ht_handler = HashtagHandler()

    def tag_text(self, text):
        """
        replaces text_to_tag tag part with appropriate tag label
        e.g. http://... ==> {LINK}
        """
        self.reset()
        for key in self._patterns_:
            if key == g.TAGS.CAPITAL:
                if re.match(self._patterns_[key], text):
                    text += self.tag_pattern % self._tag(key)
                    self._tags[g.TAGS.reverse_mapping[key]] = "yes"
            if key == g.TAGS.HT:
                ht = re.findall(self._patterns_[key], text)
                if ht != None and ht!=[]:
                    if type(ht) == list:
                        for each in ht:
                            ht_key = self.ht_handler.handle(each)
                    else:
                        ht_key = self.ht_handler.handle(ht)
                    text = re.sub(self._patterns_[key], self.tag_pattern % ht_key , text.lower())
                    self._tags[g.TAGS.reverse_mapping[ht_key]] = "yes"
            if key == g.TAGS.NEGATION:
                print "NEGATION %s" % g.TAGS.NEGATION
                print re.match(self._patterns_[key], text.lower())
                if re.match(self._patterns_[key], text.lower()):
                    self._tags[g.TAGS.reverse_mapping[key]] = "yes"
                    print "match : %s" % self._tags[g.TAGS.reverse_mapping[key]]
                text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), text.lower())
            else:
                if re.match(self._patterns_[key], text.lower()):
                    self._tags[g.TAGS.reverse_mapping[key]] = "yes"
                text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), text.lower())
        return text

    def identify_tags(self, text):
        pass

    def _tag(self, tag):
        return g.TAGS.reverse_mapping[tag]

    def reset(self):
        for key in self._tags.keys():
            self._tags[key] = "no"

