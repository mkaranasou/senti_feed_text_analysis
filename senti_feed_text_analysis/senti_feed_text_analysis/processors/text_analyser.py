# coding: utf-8
from senti_feed_text_analysis.processors.pos_tagger import POSTagger
from senti_feed_text_analysis.processors.sentiment_analyser import SentimentAnalyser
from senti_feed_text_analysis.processors.text_cleaner import TextCleaner
from senti_feed_text_analysis.processors.text_tagger import TextTagger

__author__ = 'maria'
__descr__ = 'Text Sentiment Analysis Algorithm'

from langid import langid
import senti_feed_text_analysis.helpers.globals as g


class TextAnalyser(object):
    """
    This class is meant for text handling, meaning cleaning of text and analysing sentiment
    """
    def __init__(self, sentiwordnet, stop_words_cdoulk, mysql_server_conn, top20emoticons,
                 spell_checker, calculation_methods_list, source_type, tweet, tag=False):
        ############### Main Declarations ###############
        self.sentiwordnet = sentiwordnet
        self.stop_words_cdoulk = stop_words_cdoulk
        self.mysql_server_conn = mysql_server_conn
        self.top20emoticons = top20emoticons
        self.spell_checker = spell_checker
        self.calculation_methods_list = calculation_methods_list
        self.source_type = source_type
        self.tweet = tweet
        self.tag = tag
        self.tweet.lang = self.identify_lang()
        self.pos_tagger = POSTagger()
        self.tagger = TextTagger()
        self.text_cleaner = None
        self.sentiment_analyser = None
        ################# Declarations of helpers #################
        self.spell_checker.clean_up()
        self.errors_in_text = []
        self.contains_errors = False
        self.stop_words = 0
        self.sentence_index = 0
        self.position_word_postag_score = None
        self.num_of_spell_checks_performed = 0

    def __str__(self):
        return str(self.pos_tagger.words_pos_tags)

    # begin here..
    def process_text(self):
        self.tweet.tagged_text = self.tagger.tag_text(self.tweet.text)     # sth like: i {love} this {link} {pos_smiley}
        self.tweet.tags = self.tagger._tags                                # sth like: {'LINKS':true, 'LOVE':true....}
        self.clean_text()
        self.preliminary_spellchecking()
        self.pos_tag()
        self.perform_sentiment_analysis()
        return self

    def clean_text(self):
        """
        cleaning text process
        :return:None
        """
        self.text_cleaner = TextCleaner(self.tweet, self.stop_words_cdoulk, tag=self.tag)
        self.tweet = self.text_cleaner.clean_tweet()

    def pos_tag(self):
        self.pos_tagger.pos_stem_lematize(self.tweet)

    def perform_sentiment_analysis(self):
        """
        sentiment analysis process
        :return:None
        """
        self.sentiment_analyser = SentimentAnalyser(self.tweet, self.mysql_server_conn,
                                                    self.calculation_methods_list, self.pos_tagger,
                                                    self.top20emoticons, self, self.text_cleaner)
        self.sentiment_analyser.get_emoticons_score()  # emoticons score should be calculated only once :)
        self.sentiment_analyser.calculate_emoticons_score()
        self.sentiment_analyser.process_methods()

    #TODO tbd -- should be done when a word cannot be pos tagged or found in swn
    def spell_checking(self, word):
        """
        returns the first in suggestion list correction for word.
        :param word: word that contains spelling errors
        :return:first suggestion
        """
        self.num_of_spell_checks_performed += 1

        suggestions = self.spell_checker.correct_word(word)
        g.logger.debug("SUGGESTIONS OF SPELLCHECKING {0}::: {1}".format(word, suggestions))
        if len(suggestions) >0:
            return suggestions[0]
        else:
            return word

    def preliminary_spellchecking(self):
        ''' to check if this tweet is a candidate for spellchecking
        tries to find misspelled words according to the detected language
        and stores them for future use, when a word could not be found in sentiwordnet and/or not possible to pos tag correctly
        :return: None
        '''
        #check if we have the proper dictionary
        if self.spell_checker.dict_exists(self.tweet.lang):
        # a list with possible errors is returned - one list for each sentence [[word1, word2...],[word5, word7..]]
            for word in self.tweet.words:
                if type(word) == list:  # this is in case we have more than one sentence usually.
                    for each in word:
                        if self.spell_checker.spell_checker_for_word(each) is not None:
                            self.errors_in_text.append(each)
                            g.logger.debug("TO BE SPELLCHECKED::: {0}".format(each))
                else:
                    if self.spell_checker.spell_checker_for_word(word) is not None:
                        self.errors_in_text.append(word)
                        g.logger.debug("TO BE SPELLCHECKED::: {0}".format(word))
                    else:
                        pass
            #just set the flag so we know there is room for improvement
            if self.errors_in_text.__len__() > 0:
                self.contains_errors = True
            g.logger.debug("ERRORS IN TEXT::: {0}".format(self.errors_in_text))

    #################################################### GENERAL HELPERS ###############################################
    def get_position_of_words_in_sentence(self):
        """
        Take position of words into account. Subject, Verb, Object would be a correct form.
        If Verb's or Adjective's or Adverb's position is after the identified object then,
         if no other object present, assume that they talk about the same object before them.
         Else, if there's another object, they refer to this.
        """
        enumerated_sentence = list(
            enumerate(self.words_pos_tags))  #enumerate sentence to get sth like [[1,['NN','noun']],....]
        self.position_word_postag_score = list(enumerated_sentence)
        print "SELF_POS:::", self.position_word_postag_score

    def identify_lang(self):
        """
        source for windows setup: https://pythonhosted.org/pyenchant/tutorial.html
        :return: tweet language
        """
        language = langid.classify(self.tweet.text)

        return language[0]

############################################### Main Body of processing ################################################