import traceback

import senti_feed_text_analysis.helpers.globals as g
import senti_feed_text_analysis.helpers.file_handler as fh


__author__ = 'maria'

class AspectAnalyser(object):
    def __init__(self, text_analyser):
        self.text_analyser = text_analyser

    def start_analysis_for_method(self, method):
        pass

    def get_noun_score(self, tag, word):
        noun_exists = self.text_analyser.find_if_and_where_in_list(enumerate(fh.n_v_a_r_lists[g.WORD_TYPE.NOUN]), word, 'aspect')
        if noun_exists != []:
            noun_score = fh.n_v_a_r_lists[g.WORD_TYPE.NOUN][noun_exists[0]][1][0]
            g.logger.info("{0}:{1}\t--->{2}".format(word, tag, noun_score))
        else:
            noun_score = 1.0

        return noun_exists, noun_score

    def get_aspect_var_pair_score(self, candidate, word_type):
        """
        :param word_type:
        :param candidate:
        """
        exists = self.text_analyser.find_if_and_where_in_list(enumerate(fh.n_v_a_r_lists[word_type]), candidate, 'aspect')
        if None != exists and exists != []:
            print fh.n_v_a_r_lists[word_type][exists[0]][0]
            print fh.n_v_a_r_lists[word_type][exists[0]][1]

            g.logger.debug("get_aspect_var_pair_score:::{0}".format(fh.n_v_a_r_lists[word_type][exists[0]][1]))

            return fh.n_v_a_r_lists[word_type][exists[0]][1][0]
        else:
            return None

    def get_candidates_from_bigrams(self, word):
        """

        :param word:
        :return: :rtype:
        """
        candidates = []
        for word1, word2 in self.text_analyser.bigram_tagger:
            # find pairs that noun is part of and keep the other word in order to get score afterwards
            if word1 == word and word2 not in self.text_analyser.nouns:
                candidates.append(word2)
            elif word2 == word and word1 not in self.text_analyser.nouns:
                candidates.append(word1)
        return candidates

    def get_candidate_score(self, candidates, noun_score, score_list, type):
        """
        # for each pair word found, try to get a score
        :param candidates:
        :param noun_score:
        :param score_list:
        """
        for candidate in candidates:
            tmp_score = []
            if candidate in self.text_analyser.verbs:
                g.logger.info("Verb candidate:::\t{0}\t{1}".format(candidate, self.text_analyser.verbs.index(candidate)))
                if type == 'nvar':
                    verb_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.VERB)
                    if verb_score != None and verb_score!=[]:
                        score_list.append(verb_score)
                        score_list.append(noun_score)
                        g.logger.info("Verb score:::\t{0}\t{1}".format(candidate, verb_score))
                    else:
                        pass

                elif type == 'sum':
                    tmp_score == self.text_analyser.search_db_for_sum_score(candidate)
                    if tmp_score!=[] and tmp_score!=None:
                        verb_score = tmp_score[0][0] / tmp_score[0][1]
                        score_list.append(verb_score)
                        score_list.append(noun_score)

            elif candidate in self.adjectives:
                g.logger.info("Adjective candidate:::\t{0}\t{1}".format(candidate,
                                                                        self.text_analyser.adjectives.index(candidate)))
                if type == 'nvar':
                    adj_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.ADJECTIVE)
                    if adj_score != None and adj_score!=[]:
                        score_list.append(adj_score)
                        score_list.append(noun_score)
                        g.logger.info("Adjective score:::\t{0}\t{1}".format(candidate, adj_score))

                    else:
                        # If adjective could not be found then try with adverb list
                        adv_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.ADVERB)
                        if adv_score is not None and adv_score!=[]:
                            score_list.append(adv_score)
                            score_list.append(noun_score)
                            g.logger.info("Adj/Adverb score:::\t{0}\t{1}".format(candidate, adv_score))
                elif type == 'sum':
                    tmp_score == self.text_analyser.search_db_for_sum_score(candidate)
                    if tmp_score!=[] and tmp_score!=None:
                        adj_score = tmp_score[0][0] / tmp_score[0][1]
                        score_list.append(adj_score)
                        score_list.append(noun_score)

            elif candidate in self.adverbs:
                g.logger.info("Adverb candidate:::\t{0}\t{1}".format(candidate, self.text_analyser.adverbs.index(candidate)))
                if type == 'nvar':
                    adv_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.ADVERB)
                    if adv_score != None and adv_score!=[]:
                        score_list.append(adv_score)
                        score_list.append(noun_score)
                        g.logger.info("Adverb score:::\t{0}\t{1}".format(candidate, adv_score))

                    else:
                        # try with adjective list if not in adverb list
                        adj_score = self.get_aspect_var_pair_score(candidate, g.WORD_TYPE.ADJECTIVE)
                        if adj_score != None and adv_score!=[]:
                            score_list.append(adj_score)
                            score_list.append(noun_score)
                            g.logger.info("Adv/Adjective score:::\t{0}\t{1}".format(candidate, adj_score))
                elif type == 'sum':
                    tmp_score == self.search_db_for_sum_score(candidate)
                    if tmp_score!=[] and tmp_score!=None:
                        adv_score = tmp_score[0][0] / tmp_score[0][1]
                        score_list.append(adv_score)
                        score_list.append(noun_score)

            else:
                g.logger.info("None fits candidate:::\t{0}".format(candidate))

    def calculate_aspect_score(self, pos_tagger, type):
        """
        :param pos_tagger:
        """
        final = 0.0
        all_aspect_scores = []
        temp_aspect_results = []
        tagger = pos_tagger
        aspects_list = []
        aspects_score_list = []

        print "ASPECT::::::", self.aspect

        try:
            for word, tag in tagger:
                final_aspect_score = 0.0
                score_list = []
                noun_list = []
                if tag in NOUNS and word not in noun_list:  # in order not to have multiple aspects
                    noun_list.append(word)
                    aspects_list.append(word)
                    if tag != 'NN':  # if we don't have a pure Noun, then try stemming
                        g.logger.debug("Before stemming noun:::{0}".format(word))
                        word = self.lancaster_stemmer.stem(word)
                        g.logger.debug("After stemming noun:::{0}".format(word))

                    # if tagged word is a noun then go on and check bigrams.
                    candidates = self.get_candidates_from_bigrams(word)

                    # keep aspect and unique list of matching
                    temp_aspect_results.append([word, OrderedDict.fromkeys(candidates).keys()])

                    # firstly get the Noun's score --> usually 1.0 : neutral
                    if type == 'nvar':
                        noun_exists, noun_score = self.get_noun_score(tag, word)
                    elif type == 'sum':
                        noun_exists = self.search_db_for_sum_score(word, False)
                        print "noun exists", noun_exists
                        if noun_exists!=[] and noun_exists[0][0]!=None:
                            noun_score = noun_exists[0][0]/noun_exists[0][1]
                        else:
                            noun_score = 1.0

                    # try to find for every candidate a score in corresponding V A R list and append it to score_list
                    self.get_candidate_score(candidates, noun_score, score_list, type)

                    for score in score_list:
                        final_aspect_score += float(score)
                    if score_list.__len__()>0:
                        score = final_aspect_score/float(score_list.__len__())
                    else:
                        score = noun_score

                    all_aspect_scores.append(score)
                    aspects_score_list.append(score)
                    g.logger.info("Score for aspect {0} {1} is {2}".format(noun_list[-1], candidates, score))

            g.logger.info("Aspect analysis:::\t"+str(temp_aspect_results))
            g.logger.info("Aspect - Score Lists:::\t{0} - {1}".format(aspects_list, aspects_score_list))

        except IndexError:
            g.logger.error("No trigram list.")
            traceback.print_exc()

        if all_aspect_scores.__len__()>0:
            for s in all_aspect_scores:
                try:
                    final += float(s)
                except TypeError:
                    traceback.print_exc()
            g.logger.info("Score for aspects:::{0}".format(final/all_aspect_scores.__len__()))
            return final/all_aspect_scores.__len__(), aspects_list, aspects_score_list
        return 1.0, aspects_list, aspects_score_list

    #done
    def find_if_and_where_in_list(self, list_to_search, what_to_search_for, args):
        """
        to find if sth exists in an enumerated list [[1,[1st, 2nd]],[2,[1st, 2nd]]] when we care about where.
        """
        exists = None
        if args == 1:
            exists = [a for a, (i, s) in list_to_search if i == what_to_search_for]
        elif args == 2:
            exists = [a for a, (i, s) in list_to_search if s == what_to_search_for]
        elif args == 'aspect':
            exists = [a for a, (i, s) in list_to_search if what_to_search_for in s]

        return exists