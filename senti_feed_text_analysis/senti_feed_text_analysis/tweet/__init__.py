# coding: utf-8
import traceback
import string
import re

from langid import langid
from nltk.corpus import stopwords
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize
from nltk import sent_tokenize
import nltk

import senti_feed_text_analysis.helpers.file_handler as fh
import senti_feed_text_analysis.helpers.globals as g


__author__ = 'maria'
__descr__ = 'Sentiment Analysis Algorithm'

NOUNS = ['N', 'NP', 'NN', 'NNS', 'NNP', 'NNPS']
VERBS = ['V', 'VD', 'VG', 'VN', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
ADJECTIVES = ['ADJ', 'JJ', 'JJR', 'JJS']
ADVERBS = ['RB', 'RBR', 'RBS', 'WRB']

NEGATIONS = ['no', 'don\'t', 'won\'t', 'couldn\'t', 'can\'t', 'didn\'t', 'not']

sw_list = []
swn_list = []
nvar_lists = ['n', 'v', 'a', 'r']

class TweetHandler():
    """
    This class is meant for tweet text handling, meaning cleaning of tweet and analysing sentiment
    """

    def __init__(self, tweet_id, tweet_text, sentiwordnet, stop_words_cdoulk, mysql_server_conn, top20emoticons, spell_checker):
        #assert isinstance(tweet_id, int)
        self.spell_checker = spell_checker
        self.spell_checker.clean_up()
        self.errors_in_text = []
        self.tweet_id = tweet_id
        self.tweet_text = tweet_text
        self.sentiwordnet = sentiwordnet
        self.stop_words_cdoulk = stop_words_cdoulk
        self.stop_words = 0
        self.stop_words_removed = []
        self.sentences = []
        self.words = []
        self.words_pos_tags = {}
        self.smiley_full_pattern = '(:-\)|:-\(|:-\(\(|:-D|:-\)\)|:\)\)|\(:|:\(|:\)|:D|=\)|;-\)|XD|=D|:o|:O|=]|D:|;D|:]|=/|=\(!:\'\(|:\'-\(|:\\|:/|:S|<3)'
        self.smileys_per_sentence = []
        self.sentence_index = 0
        self.reference = []
        self.links = []
        self.non_word_chars_removed = []
        self.sentence_score = []
        self.tweet_score = 0.0
        self.sentence_emoticon_score = []
        self.tweet_emoticon_score = []
        self.sentiment_eval = ''
        self.hash_list = []
        self.aspect = []
        self.mysql_server_conn = mysql_server_conn
        self.top20emoticons = top20emoticons
        self.combined_score = 0.0
        self.em = []
        self.emoticons_score = 0.0
        self.nouns = []
        self.verbs = []
        self.adjectives = []
        self.adverbs = []
        self.non_nvar = []
        self.nvar_score = 1.0
        self.non_nvar_score = 1.0
        self.nvar_combi_score = 1.0
        self.nvar_text_score = ""
        self.final_tweet = ""
        self.new_hashtag_list = []
        self.language = langid.classify(self.tweet_text)
        self.corrected_words = []
        self.contains_errors = False
        self.num_of_spell_checks_performed = 0

    def __str__(self):
        return str(self)

    # begin here..
    def tweet_processor(self):
        self.remove_non_ascii_chars()
        self.split_sentences()
        self.identify_negations()
        self.has_capitals()
        self.remove_links()
        self.store_and_remove_emoticons()
        self.remove_reference()
        self.handle_hashtags()
        self.remove_special_chars()
        self.fix_space()
        self.split_words()
        self.convert_to_lower()
        #self.remove_multiples()
        self.remove_stop_words()
        #self.preliminary_spellchecking()
        self.pos_tag_words()
        #self.trigram_pos_tag_words()
        self.get_n_v_a_r_from_pos_tag()
        self.calculate_n_v_a_r_score()
        self.calculate_non_nvar_score()
        self.calculate_text_score(text=None)
        self.calculate_combined_score()
        self.set_final_tweet()

        #print self.tweet_text, self.language[0], self.contains_errors, self.errors_in_text, \
        #                            self.non_nvar, self.non_nvar_score, self.nvar_combi_score,  self.combined_score
        #print self.final_tweet

        return self.calculate_sentiment(self.tweet_score)

    # 0: negative, 1:neutral, 2:positive etc. returns the string of evaluation
    def calculate_sentiment(self, score):
        if self.tweet_score < 0 or self.tweet_score == 1.0:
        #print("neutral")
            return "neutral"
        elif self.tweet_score > 1.0 and self.tweet_score < 1.75:
        #print("somewhat positive")
            return "somewhat positive"
        elif self.tweet_score >= 1.75:
        #print("very positive")
            return "very positive"
        elif self.tweet_score < 1.0 and self.tweet_score > 0.25:
        #print("somewhat negative")
            return "somewhat negative"
        elif self.tweet_score >= 0.0 and self.tweet_score <= 0.25:
        #print("very negative")
            return "very negative"

    # combined means text and emoticons
    def calculate_combined_score(self):
        new_list = list(enumerate(self.top20emoticons))
        for em in self.smileys_per_sentence:
            for e in em:
                exists = [n for n, (i, s) in new_list if i == e]
                if exists != None and exists != []:
                    self.em.append(new_list[exists[0]][1][1])

        for s in self.em:
            self.combined_score += s
            self.emoticons_score +=s

        if self.em.__len__() > 0:#if there is emoticons score - go on and calculate combi score for any method
            self.combined_score = ((self.combined_score / self.em.__len__()) + self.tweet_score) / 2.0
            self.nvar_combi_score = ((self.combined_score/self.em.__len__()) + self.nvar_score + self.non_nvar_score)/ 3.0
        else:
            self.combined_score = self.tweet_score
            self.nvar_combi_score = (self.nvar_score + self.non_nvar_score) /2.0

    #to solve problem with extra / weird characters when getting data from database
    def remove_non_ascii_chars(self):
        self.tweet_text = str(filter(lambda x: x in string.printable, self.tweet_text))

    #1 tokenize sentences with nltk -- will c if it needs changing
    def split_sentences(self):
        self.sentences = sent_tokenize(self.tweet_text)

    # a. finds http links, stores them in a list and then removes them from tweet text
    def remove_links(self):
        clean_list = []
        for sentence in self.sentences:
            clean_sentence = re.sub("(?P<url>https?://[^\s]+)", '', sentence)
            self.links.append(re.search("(?P<url>https?://[^\s]+)", sentence))
            clean_list.append(clean_sentence)
        self.sentences = clean_list

    # b. match all of the usual emoticons: :-), :), :D, :-D, :-(, :(, :)), :-((, :-)), (:
    def store_and_remove_emoticons(self):
        clean_list = []
        for sentence in self.sentences:
            smiley_full_patterns = re.findall(self.smiley_full_pattern, unicode(sentence))
            self.smileys_per_sentence.insert(self.sentence_index, smiley_full_patterns)
            self.sentence_index += 1 # to know in which sentence the smileys belong
            clean_sentence = re.sub(self.smiley_full_pattern, '', sentence)
            clean_list.append(clean_sentence)
        self.sentences = clean_list

    # c. stores reference in a list and then removes it from tweet text
    #TODO decide upon handling
    def remove_reference(self):
        clean_list = []
        for sentence in self.sentences:
            self.reference.append(re.findall('(@\w+|@\D\w+|@\w+\D)', sentence))
            self.hash_list.append(re.findall('(#\w+|#\D\w+|#\w+\D)', sentence))
            #clean_sentence = re.sub('(@\w+|@\D\w+|@\w+\D|#\w+|#\D\w+|#\w+\D)', '', sentence)
            clean_sentence = re.sub('(@|#)', '', sentence)
            clean_list.append(clean_sentence)
        self.sentences = clean_list

    # d. will store and remove any characters like '?' '!!!' '...' to see if we can infer some meaning for sentiment
    def remove_special_chars(self):
        clean_list = []
        for sentence in self.sentences:
            self.non_word_chars_removed.append(re.findall(r'\W|\d|_', sentence))
            clean_sentence = re.sub(r'\W|\d|_', ' ', sentence)
            clean_list.append(clean_sentence)
        self.sentences = clean_list

    # e. remove stop words - both from ntlk.stop_words and from cdoulk_file
    #TODO check if it is done correctly
    def remove_stop_words(self):
        clean_word_list = []
        for word in self.words:
            #for i in range(0, word.__len__()):
            try:
                if any(word in c for c in self.stop_words_cdoulk):
                    clean_word_list.append(word)
                    self.stop_words_removed.append(word)
                elif any(word.encode() in s for s in stopwords.words()):
                    clean_word_list.append(word.decode('utf-8'))
                    self.stop_words_removed.append(word.decode())
            except:
                print "problem in stop - word removal" + str(Exception)
                traceback.print_exc()
            for item in clean_word_list:
                if item in self.words:
                    self.words.remove(item)

    #TODO find how to remove multiple characters correctly
    #assume that two characters in a row are valid. If not they should be corrected in spell-checking
    def remove_multiples(self):
        counter = 0
        final_word = ''
        print self.words.__len__()
        for l in range(0, self.words.__len__()):
            for word in self.words[l]:
                for i in range(1, word.__len__()):
                    if word[i - 1] == word[i]:
                        counter += 1
                    else:
                        if counter > 2:
                            print "duplicate indicator, do sth"
                            for i in range(counter - 2, counter + 1):
                                '''heeeeelloooo'''
                                final_word += word[:i - counter + 1] + word[counter + 1:]
                        counter = 0
                        #print final_word
                        #print word
                        #remove duplicates.... not quite
                        #from collections import OrderedDict
                        #foo = "heeeelloooo"
                        #print "".join(OrderedDict.fromkeys(foo))

    #5 split words in sentence
    def split_words(self):
        for sentence in self.sentences:
            try:
                self.words.append(word_tokenize(sentence.decode(encoding='UTF-8')))
            except:
                self.words.append(pos_tag(word_tokenize(sentence)))

    # reduces multiple whitespace characters into a single space.
    def fix_space(self):
        clean_list = []
        for sentence in self.sentences:
            clean_sentence = ' '.join(sentence.split())
            clean_list.append(clean_sentence)
        self.sentences = clean_list

    # get pos tag for every valid word in each sentence
    def pos_tag_words(self):
        try:
            #fixed bug 2-2-2013
            self.words_pos_tags.update(pos_tag(self.words))
        except:
            traceback.print_exc()
            print "could not pos tag:" + self.words

    def trigram_pos_tag_words(self):
        tagged_corpora=[]
        for word in self.words:
            tagged_corpora.append(pos_tag(word))#tagged sentences with pos_tag
            #tagged_corpora.append(pos_tag_results)
        print(tagged_corpora)
        try:
            trigram_tagger=nltk.TrigramTagger(tagged_corpora) #build trigram tagger based on your tagged_corpora
            trigram_tag_results=trigram_tagger(self.words) #tagged sentences with trigram tagger
            for i in range(0,len(tagged_corpora)):
                    if tagged_corpora[i][1]=='NN':
                        tagged_corpora[i][1]=trigram_tag_results[i][1]#for 'NN' take trigram_tagger instead
            print tagged_corpora
        except:
            traceback.print_exc()

    # the aspect of the tweet could be NN
    #TODO it has been incorporated in get n v a r. clean or remove this one.
    def get_aspect_from_pos_tag(self):
        for key in self.words_pos_tags.keys():
            if self.words_pos_tags[key] == 'NN' or self.words_pos_tags[key] == 'NP':
                self.aspect.append(key)
                #pos_tag_results[i][1]=trigram_tag_results[i][1]#for 'NN' take trigram_tagger instead

    #store nouns, verbs, adjectives, adverbs from pos tag in lists in order to check for score in respective files
    def get_n_v_a_r_from_pos_tag(self):
        for key in self.words_pos_tags.keys():
            if self.words_pos_tags[key] in NOUNS:
                self.nouns.append(key)
                self.aspect.append(key) # moved from get_aspect to simplify
                '''if self.words_pos_tags[key] == 'NN' or self.words_pos_tags[key] == 'NP':
                    print(key)'''
            elif self.words_pos_tags[key] in VERBS:
                self.verbs.append(key)
            elif self.words_pos_tags[key] in ADJECTIVES:
                self.adjectives.append(key)
            elif self.words_pos_tags[key] in ADVERBS:
                self.adverbs.append(key)
            else:
                self.non_nvar.append(key)

    #to find if sth exists in an enumerated list [[1,[1st, 2nd]],[2,[1st, 2nd]]] when we care about where.
    def find_if_and_where_in_list(self, list_to_search, what_to_search_for, first_or_second_arg):
        if first_or_second_arg == 1:
            exists = [a for a, (i, s) in list_to_search if i == what_to_search_for]
        elif first_or_second_arg == 2:
            exists = [a for a, (i, s) in list_to_search if s == what_to_search_for]

        return exists

    #supposing that pos tagging works well
    #we calculate the score from each word in lists
    def calculate_n_v_a_r_score(self):
        #enumerate the lists in order to be able to get the position when score is found
        enumerated_lists = fh.n_v_a_r_lists
        n_v_a_r_lists = self.prepare_nvar_lists()
        score_list = []
        score = 0.0
        #for the four lists noun, verb, adjective, adverb
        for nvar_list in enumerated_lists:
            for item in n_v_a_r_lists[enumerated_lists.index(nvar_list)]:
                exists = self.find_if_and_where_in_list(nvar_list, item, 2)
                if exists != None and exists != []:
                    score_list.append(nvar_list[exists[0]][1][0])
                else:
                    pass

        if score_list.__len__() > 0:
            for i in score_list:
                score += float(i)
            self.nvar_score = score / score_list.__len__()
        else:
            print "no NVAR score"

    def calculate_non_nvar_score(self):
        non_nvar_temp_score = 0.0
        score = self.calculate_text_score(self.non_nvar)
        if score.__len__() > 0:
            for s in score:
                if not s == None:
                    non_nvar_temp_score += s
            if non_nvar_temp_score != 0.0:# We do have Score!
                self.non_nvar_score= non_nvar_temp_score / score.__len__()
            else: # NO SCORE -- todo peritto...
                self.non_nvar_score = 1.0

    #TODO this needs to be done ONCE and not here --> move it to filehandler!
    def prepare_enum_lists(self):
        return fh.n_v_a_r_lists

    def prepare_nvar_lists(self):
        return [self.nouns, self.verbs, self.adjectives, self.adverbs]

    #TODO tbd -- should be done after removing duplicates and when a word cannot be pos tagged or found in swn
    def spell_checking(self, word):
        '''for sentence in self.errors_in_text:
            for word in sentence:
                self.corrected_words.append(sp_ch.correct(word))'''
        self.num_of_spell_checks_performed += 1
        suggestions = self.spell_checker.correct_word(word)
        #print suggestions
        return suggestions[0]

    def calculate_score(self):
        pass

    #calculates the whole text score.
    #TODO: keep score per sentence
    def calculate_text_score(self, text):
        score = []
        index = 0.0
        final_score = 0.0
        temp_score = None
        #TODO make it a one time transaction ?
        if text==None:#if called with no arguments
            for word in self.words:
                if word.__len__() > 2:
                    temp_score = self.mysql_server_conn.execute_query(g.sum_query.format(word))
                    if not temp_score[0][0] == None:
                        score_per_times = temp_score[0][0] / temp_score[0][1]
                        score.append(score_per_times)
                    #if no score could be found for word, try spell checking -- duplicate code todo: refactor
                    elif self.contains_errors:
                        try:
                            temp_score = self.mysql_server_conn.execute_query(g.sum_query.format(self.spell_checking(word)))
                            if not temp_score[0][0] == None:
                                score_per_times = temp_score[0][0] / temp_score[0][1]
                                score.append(score_per_times)
                        except:
                            score.append(1.0)
                self.sentence_score = score

                #TODO Too complicated for no reason. Make it cleaner
                if self.sentence_score.__len__() > 0:
                    for s in self.sentence_score:
                        if not s == None:
                            final_score += s
                            index += 1.0
                if index > 0:# We do have Score!
                    self.tweet_score = final_score / index
                else: # NO SCORE
                    self.tweet_score = 1.0
        else:
            for word in text:
                if word.__len__() > 2:
                    try:
                        temp_score = self.mysql_server_conn.execute_query(g.sum_query.format(word))
                        if not temp_score[0][0] == None:
                            score_per_times = temp_score[0][0] / temp_score[0][1]
                            score.append(score_per_times)
                        #if no score could be found for word, try spell checking -- duplicate code todo: refactor
                        elif self.contains_errors:
                            temp_score = self.mysql_server_conn.execute_query(g.sum_query.format(self.spell_checking(word)))
                            if not temp_score[0][0] == None:
                                score_per_times = temp_score[0][0] / temp_score[0][1]
                                score.append(score_per_times)
                    except:
                        traceback.print_exc()
            return score

    #to check if this tweet is a candidate for spellchecking
    #tries to find misspelled words according to the detected language
    #and stores them for future use, when a word could not be found in sentiwordnet and/or not possible to pos tag correctly
    def preliminary_spellchecking(self):
        #check if we have the proper dictionary
        if self.spell_checker.dict_exists(self.language[0]):
            for word in self.words:
                #a list with possible errors is returned - one list for each sentence [[word1, word2...],[word5, word7..]]
                if self.spell_checker.spell_checker_for_word(word) is not None:
                    self.errors_in_text.append(word)
                else:
                    pass
                    #print("No spelling problems with", word)
            #just set the flag so we know there is room for improvement
            if self.errors_in_text.__len__()>0:
                self.contains_errors = True

    def calculate_tweet_score(self):
        pass

    #convert every word to lowercase to have better results in matching
    def convert_to_lower(self):
        temp_wordlist = []
        for i in self.words:
            for word in i:
                temp_wordlist.append(word.lower())
        self.words = temp_wordlist

    #to try get the aspect out of hashtags -- just to have some comparison for pos tag aspect
    #assumption is that hashtag will be splittable by capitals //Pascal or camelCase like
    def handle_hashtags(self):
        for hashtag in self.hash_list:
            self.new_hashtag_list.append([a for a in re.split(r'([A-Z][a-z]*\d*)', str(hashtag)) if a])

    def set_final_tweet(self):
        for word in self.words:
            self.final_tweet += word + " "

    def identify_negations(self):
        pass

    def has_capitals(self):
        pass

    def get_position_of_words_in_sentence(self, enumerated_sentence=[]):
        '''
        take position of words into account. Subject, Verb, Object would be a correct form.
        If Verb's or Adjective's or Adverb's position is after the identified object then,
         if no other object present, assume that they talk about the same object before them.
         Else, if there's another object, they refer to this.
        '''
        for s in self.words_pos_tags:
            enumerated_sentence.append(list(enumerate(s)))#enumerate sentence to get sth like [[1,['NN','noun']],....]
        print enumerated_sentence
        pass

    def analyse_position_of_words_in_sentence(self):
        pass

    def process_non_word_characters(self):
        for char in self.non_word_chars_removed:
            for c in char:
                if char == '!':
                    pass
                if char =='?':
                    pass
                else:
                    pass