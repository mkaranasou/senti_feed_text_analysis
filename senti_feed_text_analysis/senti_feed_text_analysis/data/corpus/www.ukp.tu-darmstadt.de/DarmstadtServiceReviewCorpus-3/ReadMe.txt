This folder contains:
1. The annotation manual used in this study. 
2. Guidelines for using the MMAX annotation tool used in this study. 
	2.1. MMAXShortHowTo.doc
	2.2. mmaxquickstart.pdf
MMAX can be downloaded from:
http://sourceforge.net/projects/mmax2/files/
3. zipped folders containing the annotated reviews:
	3.1. universities: contains the reviews about online universities. 
	3.2. services: contains 5 subfolders. Each folder name is the topic of the reviews in the corresponding folder. 

Example structure of a mmax project:

- project.mmax: project file
- basedate: folder containing the review text and the tokenized version of the text. 
- markables: folder contains the annotations. 
- scheme: folder contains the annotation scheme. 



