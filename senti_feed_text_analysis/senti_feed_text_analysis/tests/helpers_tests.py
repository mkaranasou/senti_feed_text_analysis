import unittest

from senti_feed_text_analysis.helpers.scale_converter import ScaleConverter


__author__ = 'maria'

class ScaleConverterTester(unittest.TestCase):

    def setUp(self):
        self._scale_converter = ScaleConverter(2.0, 0.0, 5.0, -5.0)

    def test_scale_converter_neutral(self):
        self.assertTrue(self._scale_converter.convert(1.0) == 0.0)

    def test_scale_converter_max_positive(self):
        self.assertTrue(self._scale_converter.convert(2.0) == 5.0)

    def test_scale_converter_max_negative(self):
        self.assertTrue(self._scale_converter.convert(0.0) == -5.0)


class LanguageIdentificationTester(unittest.TestCase):
    def setUp(self):
        pass
