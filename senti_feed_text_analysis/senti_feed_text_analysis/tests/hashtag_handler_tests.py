import unittest
from senti_feed_text_analysis.processors.hashtag_handler import HashtagHandler
import re
from senti_feed_text_analysis.helpers import globals as g

__author__ = 'maria'


class HashtagHandlerTester(unittest.TestCase):

    def setUp(self):
        self.hastag_handler = HashtagHandler()

    def test_all_capitals_correct_hashtag(self):
        text = "not knowing how to act around someone has to be the best feeling ever #NOT"
        ht = re.findall(g.CAPITALS_PATTERN, text)
        print ht
        result = self.hastag_handler.handle(ht[0])
        print result

        self.assertEquals(g.TAGS.HT_NEG, result)

    def test_all_capitals_incorrect_spelling_hashtag(self):
        text = "not knowing how to act around someone has to be the best feeling ever #NOTT"
        ht = re.findall(g.CAPITALS_PATTERN, text)
        print ht
        result = self.hastag_handler.handle(ht[0])
        print result

        self.assertEquals(g.TAGS.HT_NEG, result)

    def test_with_capitals_correct_spelling_hashtag(self):
        text = "not knowing how to act around someone has to be the best feeling ever #NotGoing"
        ht = re.findall(g.HT_PATTERN, text)
        print ht
        result = self.hastag_handler.handle(ht[0].replace("#",""))
        print result

        self.assertEquals(g.TAGS.HT_NEG, result)

    def test_with_capitals_incorrect_spelling_hashtag(self):
        text = "not knowing how to act around someone has to be the best feeling ever #NotGoin"
        ht = re.findall(g.HT_PATTERN, text)
        print ht
        result = self.hastag_handler.handle(ht[0].replace("#",""))
        print result

        self.assertEquals(g.TAGS.HT_NEG, result)

    def test_all_capitals_incorrect_spelling_hashtag(self):
        text = "not knowing how to act around someone has to be the best feeling ever #NOTT"
        ht = re.findall(g.CAPITALS_PATTERN, text)
        print ht
        result = self.hastag_handler.handle(ht[0])
        print result

        self.assertEquals(g.TAGS.HT_NEG, result)

    def test_with_capitals_correct_spelling_hashtag_pos(self):
        text = "not knowing how to act around someone has to be the best feeling ever #LoveDoing"
        ht = re.findall(g.HT_PATTERN, text)
        print ht
        result = self.hastag_handler.handle(ht[0].replace("#",""))
        print result

        self.assertEquals(g.TAGS.HT_POS, result)

    def test_with_capitals_incorrect_spelling_hashtag_pos(self):
        text = "not knowing how to act around someone has to be the best feeling ever #LoveGoin"
        ht = re.findall(g.HT_PATTERN, text)
        print ht
        result = self.hastag_handler.handle(ht[0].replace("#",""))
        print result

        self.assertEquals(g.TAGS.HT_POS, result)

    def test_with_capitals_correct_spelling_hashtag_neg(self):
        text = "not knowing how to act around someone has to be the best feeling ever #LoveNotGoing"
        ht = re.findall(g.HT_PATTERN, text)
        print ht
        result = self.hastag_handler.handle(ht[0].replace("#",""))
        print result

        self.assertEquals(g.TAGS.HT_NEG, result)