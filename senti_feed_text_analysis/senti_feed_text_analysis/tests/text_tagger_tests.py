import unittest
from senti_feed_text_analysis.processors.text_tagger import TextTagger
from senti_feed_text_analysis.helpers import globals as g

__author__ = 'maria'

select = '''
SELECT ManualTwitterCorpus.id,
       ManualTwitterCorpus.TweetText
FROM ManualTwitterCorpus limit 100000;'''

select_twitterv2='''SELECT  TwitterCorpusV2.id,
                            TwitterCorpusV2.TweetText
FROM TwitterCorpusV2;'''

select_no_emoticons = '''
SELECT TwitterCorpusNoEmoticons.id,
       TwitterCorpusNoEmoticons.TweetText
FROM TwitterCorpusNoEmoticons;'''

update = ''' UPDATE ManualTwitterCorpus
SET TaggedText ="{0}" WHERE id = {1};'''

update_twitterv2 = '''
UPDATE TwitterCorpusV2
SET TaggedText ="{0}" WHERE id = {1};
'''
update_no_emoticons = '''
UPDATE TwitterCorpusNoEmoticons
SET TaggedText ="{0}" WHERE id = {1};
'''

select_figurative_data = '''
SELECT FigurativeTrainingData.TweetId, FigurativeTrainingData.TweetText FROM FigurativeTrainingData;
'''

update_figurative_data = '''
UPDATE FigurativeTrainingData
SET
TaggedText = "{0}"
WHERE id = "{1}";
'''


class TextTaggerTester(unittest.TestCase):

    def setUp(self):
        self.text = ""
        self.tagger = TextTagger()
    def test_figurative_data_tag(self):
        self.data = g.mysql_conn.execute_query(select_figurative_data)
        print len(self.data)
        for tweet in self.data:
            result = self.tagger.tag_text(tweet[1])
            print tweet[1]
            print result
            print self.tagger._tags
            # try:
            #     #print update_figurative_data.format(result.encode('utf-8'), tweet[0])
            #     g.mysql_conn.update(update_figurative_data.format(result.encode('utf-8'), tweet[0]))
            #     print tweet[1].encode('utf-8')
            #     print result.encode('utf-8')
            # except:
            #    traceback.print_exc()