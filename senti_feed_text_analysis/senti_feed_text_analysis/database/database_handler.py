# coding: utf-8

__author__ = 'maria'

#Establish connection to DB
#conn = pypyodbc.connect('DRIVER={MySQL ODBC 5.1 Driver};SERVER=localhost;DATABASE=SentiFeed;UID=root;PWD=291119851$Mk', ansi=True)
'''
class MySQLDatabaseConnector(object):
    def __init__(self):
        try:
            self.conn = mysql.connector.connect(user='root', password='291119851$Mk',
                              host='127.0.0.1',
                              database='SentiFeed')
            self.query = ''
            self.cur = self.conn.cursor()
        except:
            traceback.print_exc()
            print "Could not connect to MySQL, check if server is running!"


    def execute_query(self, query):
        self.query = query
        self.cur.execute(query)
        rows = self.cur.fetchall()
        return rows

    def update(self,query):
        self.query = query
        self.cur.execute(self.query)
        self.conn.commit()

    def close_conn(self):
        self.conn.close()

class SQLServerDbConnector(object):
    def __init__(self):
        self.conn = pypyodbc.connect('DRIVER={SQL Server};SERVER=87.203.107.11:1433;DATABASE=TweetFeed;UID=gen_purpose_user;PWD=291119851$Mk')
        self.query = ''
        self.cur = self.conn.cursor()

    def execute_query(self, query):
        self.query = query
        self.cur.execute(query)
        rows = self.cur.fetchall()
        return rows

    def update(self,query):
        self.query = query
        test = self.cur.execute(self.query)
        return test


    def close_conn(self):
        self.conn.close()

# todo: need to create insert/create metods
class MongoDBConnector(object):
    def __init__(self, db):
        self.client = MongoClient('localhost', 27017)
        self.db_names = self.client.database_names()
        if db in self.db_names:
            print self.db_names
            self.db = self.db_names[self.db_names.index(db)]
        self.collection = None

    def get_collection(self, collection):
        self.collection = self.db[collection]

    def save_document_in_collection(self, document, collection):
        self.document = document
        self.document_id = self.db.collection.insert(self.document)

#ObjectId method is used to convert string id to object id
    def retrieve_document_by_id(self,id):
        self.document = self.db.collection.find_one({"_id": ObjectId(id)})

#item must be properly formated in JSON
    def retrieve_document_by(self,item):
        self.document = self.db.collection.find_one({item})


class RedisConnector(object):
    def __init__(self):
        try:
            self.pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
            self.redis_server = redis.Redis(connection_pool=self.pool)
            self.tweet_q = 'tweet_queue'
        except redis.ConnectionError:
            traceback.print_exc()
            g.logger.error("Could not connect to Redis...:{0}".format(redis.ConnectionError))
            return False

    def get_variable(self, variable_name):
        response = self.redis_server.get(variable_name)
        return response

    def set_variable(self, variable_name, variable_value):
        self.redis_server = redis.Redis(connection_pool=self.pool)
        self.redis_server.set(variable_name, variable_value)

    def store_tweet_first_in_queue(self, tweet):
        redis.Redis.rpush(self.redis_server, self.tweet_q, tweet)

    def get_tweet_last_in_q(self):
        return redis.Redis.rpop(self.redis_server, self.tweet_q)

    def get_tweet_list_len(self):
        return redis.Redis.llen(self.redis_server, self.tweet_q)
'''
###############################################################################
#test
'''doc = {
    id:1,
    'text': 'test'
}
mongo = MongoDBConnector('sentifeed')

mongo.save_document_in_collection(doc, None)'''

'''mysql_server_conn = MySQLDatabaseConnector()
rows = mysql_server_conn.execute_query("SELECT * FROM SampleTweets LIMIT 1000")

for row in rows:
    try:
        tweet_id = str(row[2])
        tweet= row[3]
        print tweet_id, str(tweet).strip('\t\r\n')
    except:
        print row[3]


mysql_server_conn.close_conn()'''