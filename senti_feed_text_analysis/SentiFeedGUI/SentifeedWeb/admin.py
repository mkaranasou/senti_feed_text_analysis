from django.contrib import admin
from models import Tweet1, Emotion

# Register your models here.
admin.site.register(Tweet1)
admin.site.register(Emotion)