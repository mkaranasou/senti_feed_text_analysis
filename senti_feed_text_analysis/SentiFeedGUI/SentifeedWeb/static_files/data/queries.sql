UPDATE SentiFeed.TwitterCorpusV2, SentiFeed.TwitterCorpus
SET SentiFeed.TwitterCorpusV2.InitialAspect = SentiFeed.TwitterCorpus.Aspect
WHERE SentiFeed.TwitterCorpusV2.TweetId = SentiFeed.TwitterCorpus.id;

UPDATE `SentiFeed`.`CalculationMethod`
SET
`Accuracy` = {0},
`CombinedAccuracy` = <{CombinedAccuracy: 0}>,
`AspectAccuracy` = <{AspectAccuracy: 0}>,
`AspectScoreAccuracy` = <{AspectScoreAccuracy: 0}>,
`DateLastEvaluated` = <{DateLastEvaluated: }>
WHERE `id` = <{expr}>;


CREATE TABLE SentiFeed.ManualTwitterCorpus
(
    id int PRIMARY KEY NOT NULL,
    TweetId bigint  NOT NULL,
    TweetText varchar (140) NOT NULL,
    CreateDate varchar (150) NOT NULL,
    Query varchar (100) NOT NULL,
    User varchar (100) NOT NULL
);
ALTER TABLE SentiFeed.ManualTwitterCorpus ADD CONSTRAINT unique_id UNIQUE (id);
ALTER TABLE `SentiFeed`.`ManualTwitterCorpus`
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;


CREATE TABLE `TwitterCorpusNoEmoticons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TweetId` bigint(20) NOT NULL,
  `TweetText` varchar(140) NOT NULL,
  `CreateDate` varchar(150) NOT NULL,
  `Query` varchar(100) NOT NULL,
  `User` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `SentiFeed`.`Source` (`SourceTable`) VALUES ('TwitterCorpus');
INSERT INTO `SentiFeed`.`Source` (`SourceTable`) VALUES ('ManualTwitterCorpus');
INSERT INTO `SentiFeed`.`Source` (`SourceTable`) VALUES ('TwitterCorpusNoEmoticons');


CREATE TABLE `SentiFeed`.`ProcessedTweets` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `TweetId` BIGINT NOT NULL,
  `TweetInitial` VARCHAR(150) NULL,
  `TweetProcessed` VARCHAR(150) NULL,
  `Hashtags` VARCHAR(150) NULL,
  `Capitals` VARCHAR(150) NULL,
  `Negations` VARCHAR(150) NULL,
  PRIMARY KEY (`id`, `TweetId`));
