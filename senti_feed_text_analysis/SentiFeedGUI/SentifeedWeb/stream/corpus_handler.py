import traceback
import database

__author__ = 'maria'

corrected = open('../data/stream/corrected.txt', 'r')
corpus = open('../data/stream/corpus.csv', 'r')
corpus_list = []
conn = database.MySQLDatabaseConnector()

query = '''INSERT INTO `SentiFeed`.`TwitterCorpus` (`id`, `TweetText`, `DateAdded`, `UserId`, `UserName`, `Aspect`, `HandcodedEmotion`) VALUES ("{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}");'''

for line in corpus.readlines():
    corpus_list.append(line.split(','))


def corpus_handler(line):
    #for c in corpus_list:
    for c in corpus_list:
        if c[2].strip('\n""') == line[0]:
            print query.format(line[0], str(line[1]), str(line[2].replace('"', ' ')).strip("'"), str(line[4]).strip('"\''), str(line[5].strip('"')), str(c[0]).strip('"'),
                         str(c[1]).strip('"'))
            try:
                conn.update(query.format(line[0], str(line[1]), (str(line[2]).replace('"', ' ')).strip("'"), str(line[4]).strip('"\''), str(line[5].strip('"')), str(c[0]).strip('"'),
                         str(c[1]).strip('"')))
            except:
                traceback.print_exc()


for line in corrected.readlines():
    corpus_handler(line.split(",\t"))
