from threading import Thread

from datetime import time
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener

from senti_feed_text_analysis.database import RedisConnector
import senti_feed_text_analysis.helpers.globals as g


__author__ = 'maria'
__descr__ = 'To retrieve data from twitter stream and store them in files or db'
__source__ = 'http://stackoverflow.com/questions/12967107/managing-connection-to-redis-from-python'

#file_to_write = open('../data/stream/greek_elections.txt', 'a')
stopped = False
redis_conn = RedisConnector()


#to handle streaming data
class StreamHandler(object):
    def __init__(self):
        self.oauth_ = OAuthHandler(g.ckey, g.csecret)
        self.oauth_.set_access_token(g.atoken, g.asecret)
        self.twitterStream = Stream(self.oauth_, Listener())

    def start_listening(self, term, area=None, async=True):
        global stopped
        stopped = False
        self.twitterStream.timeout = 1000
        if area!=None:
            self.twitterStream.filter(track=[term], locations=[area], async=async)
        else:
            self.twitterStream.filter(track=[term], async=async)


    def stop_listening(self):
        global stopped
        self.twitterStream.disconnect()
        stopped = True

class Listener(StreamListener):
    """
    Overriding StreamListener methods in order to get a hold on data
    """
    def on_data(self, data):
        if not stopped:
            self.save_in_redis(data)
            return True
        else:
            return False

    def on_error(self, status_code):
        g.logger.error("Error in stream:{0}".format(status_code))

    def save_in_redis(self, data):
        if redis_conn:
            redis_conn.store_tweet_first_in_queue(data)
            print 'in redis %s' % data
        else:
            g.logger.error("Could not save in redis:::{0}".format(data))

stream_handler = StreamHandler()
thread = Thread(target=stream_handler.start_listening('Love'))
thread.start()
print "thread {0} for {1} just started!".format(str(thread.name))
time.sleep(20)
print 'still up'

#probably never going to go through this but eitherway! :P
#file_to_write.close()

#import sys
#sys.stdout = open('valentines.txt', 'a')

#twitter_list = twitterStream.filter(track=["New Year"])
#with open('stream_data.txt', 'w') as myFile:
#    myFile.write(str(twitterStream.filter(track=["2014"])))
    #myFile.close()
#file = open("newyears.txt", "a")
#file.write(twitterStream.filter(track=["New Year"]))
#file.close()

# This takes a python object and dumps it to a string which is a JSON representation of that object
#data = json.load(twitterStream.filter(track=["Greece"]))
#unpacked_data = json.dump(twitterStream.filter(track=["Greece"]),indent=2)
#for tweet in data:
#print data.text


#with open('stream_data.txt', 'r') as outfile:
#  simplejson.dump(twitterStream, outfile)

#data = {}
#with open('stream_data.txt') as f:
#    for line in f:
#        print line
#        data = json.loads(line,None,None,'text')

    #data = json.loads(test_file)
#for key,value in data.items():
#    print key, value

