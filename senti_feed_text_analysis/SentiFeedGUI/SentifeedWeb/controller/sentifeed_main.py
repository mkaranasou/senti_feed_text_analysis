import datetime
import string
import traceback
from SentifeedWeb import database
from SentifeedWeb.evaluator import Evaluator
from SentifeedWeb.helpers import globals as g
from SentifeedWeb.helpers import file_handler as fh
from SentifeedWeb.models import CalculationMethod, StatisticsHandler, TwitterCorpus, Tweet
from SentifeedWeb.processors.text_analyser import TextAnalyser
import SentifeedWeb.helpers.pyenchant_spell_checker as sp_ch

__author__ = 'maria'


class SentiFeed(object):
    def __init__(self, db_type, corpus, swn, stop_words, emoticons, corpus_table_id, evaluation=True):
        self._conn = self._get_database_connection(db_type)
        self.corpus_table_id = corpus_table_id
        self.essential_data_list = [corpus, swn, stop_words, emoticons]
        self.data_list = self._get_essential_data()
        self.spell_checker = self.get_spellchecker()
        self.method_list = self.get_methods_list()
        self.tweet_obj_list = []
        self.ta = None
        self.evaluation = evaluation
        self.score_list =[] # list of score objects like: [[score_of_method1, score_of_method2....], [  ...  ]]
                            #                               <----score of 1 tweet --------------->  <--2nd tweet-->


    def print_essentials(self):
        print(datetime.datetime.now())


    def _get_database_connection(self, db_type):
        #todo: take db_type into account
        if db_type == g.DB_TYPE.MYSQL:
            return database.MySQLDatabaseConnector()
        else:
            g.logger.error("Not implemented DB connection")
            raise NotImplementedError

    def _get_essential_data(self):
        corpus = self.gather_data(self.essential_data_list[0])
        swn = self.gather_data(self.essential_data_list[1])
        stop_words = self.gather_data(self.essential_data_list[2])
        emoticons = self.gather_data(self.essential_data_list[3])

        return [corpus,swn, stop_words, emoticons]

    def gather_data(self, data_type):
        g.logger.info('gather_data ' + g.DATA_TYPE.reverse_mapping[data_type])
        if data_type == g.DATA_TYPE.TWITTER_CORPUS:
            #return self._conn.execute_query(g.select_from_twitter_corpus.format(limit=10))
            return self.get_corpus_object_list()
            g.logger.info('g.select_from_twitter_corpus.format(limit=10)')
        elif data_type == g.DATA_TYPE.SWN:
            return self._conn.execute_query(g.select_from_swn)
        elif data_type == g.DATA_TYPE.STOP_WORDS:
            return self._conn.execute_query(g.select_from_stop_words)
        elif data_type == g.DATA_TYPE.EMOTICONS:
            return self._conn.execute_query(g.select_from_top_emoticons)
        else:
            g.logger.error("Not implemented DATA TYPE method")
            raise NotImplementedError

    def clean_data_from_db(self):
        #fixme:this probably won't work well due to assignment to each...
        temp_list = []
        for data in self.data_list:
            '''if self.data_list.index(data) == self.essential_data_list.index(g.DATA_TYPE.SWN):
                g.logger.info("clean_data_from_db:::" + g.DATA_TYPE.reverse_mapping[g.DATA_TYPE.SWN])
                for row in data:
                    for each in row:
                        each = filter(lambda x: x in string.printable, str(each))
                    temp_list.append(row)
                    #    type = filter(lambda x: x in string.printable, str(each[0]))
                    #    synset = filter(lambda x: x in string.printable, str(each[1]))
                    #temp_list.append((type, synset, each[2]))
                return temp_list'''
                #clean stop words list data fetched from db
            if self.data_list.index(data)==self.essential_data_list.index(g.DATA_TYPE.STOP_WORDS):
                g.logger.info("clean_data_from_db:::" + g.DATA_TYPE.reverse_mapping[g.DATA_TYPE.STOP_WORDS])
                for sw in data:
                    test = (str(sw[0]).replace('\r', '').replace(',', ''))
                    stop_word = filter(lambda x: x in string.printable, test)
                    temp_list.append(stop_word)
            else:
                pass

    def handle_files(self):
        g.logger.info("handle_files")
        fh.handle_files()

    def prepare_lists(self):
        g.logger.info("prepare_lists")
        fh.prepare_enum_lists()

    def get_data_list_item(self, data_list_item):
        return self.data_list[self.essential_data_list.index(data_list_item)]

    def get_spellchecker(self):
        return sp_ch.EnchantSpellChecker()

    def get_text_analyser(self, row):
        if isinstance(row, str):
            #g.logger.info('str')
            tweet = Tweet()
            tweet.text = row  # text only
            return TextAnalyser(self.get_data_list_item(g.DATA_TYPE.SWN),
                                      self.get_data_list_item(g.DATA_TYPE.STOP_WORDS),
                                      self._conn, self.get_data_list_item(g.DATA_TYPE.EMOTICONS),
                                      self.spell_checker, self.method_list, None, tweet)
        elif isinstance(row, tuple):
            #g.logger.info('[]')
            tweet = Tweet()
            tweet.id = row[0]  # tweet id
            tweet.text = row[1] # tweet text
            return TextAnalyser(self.get_data_list_item(g.DATA_TYPE.SWN),
                                      self.get_data_list_item(g.DATA_TYPE.STOP_WORDS),
                                      self._conn, self.get_data_list_item(g.DATA_TYPE.EMOTICONS),
                                      self.spell_checker, self.method_list, None, tweet)
        elif isinstance(row, TwitterCorpus):
            #g.logger.info('[]')
            tc = row
            tweet = Tweet()
            tweet.id = tc.tweet_id
            tweet.text = tc.tweet_text
            return TextAnalyser(self.get_data_list_item(g.DATA_TYPE.SWN),
                                      self.get_data_list_item(g.DATA_TYPE.STOP_WORDS),
                                      self._conn, self.get_data_list_item(g.DATA_TYPE.EMOTICONS),
                                      self.spell_checker, self.method_list, None, tweet)
        else:#todo: for future use, add twitter streaming implementation
            g.logger.error("Text Analyser cannot be initialised for this kind of data:::"+str(type(row)))
            raise NotImplementedError

    def update_db_with_score(self, score, ta):
        try:
            new_id = filter(lambda x: x in string.printable, str(ta.id))
            final_tweet = self.prepare_save_query(ta.words)
            self._conn.update(g.update_twitter_corpus_q.format(str(score), str(final_tweet),
                                                                           str(ta.final_emoticons),
                                ta.tweet_score, ta.combined_score, ta.emoticons_score, ta.nvar_score,
                                ta.nvar_combi, str(new_id).strip('\n')))
        except:
            pass

    @staticmethod
    def prepare_save_query(text_to_clean):
        """

        :param text_to_clean:
        :return: :rtype:
        """
        final_text = ""
        if isinstance(text_to_clean, list):
            for text in text_to_clean:
                for i in range(0, text.__len__()):
                    final_text = str(text[i]) + " "
        elif isinstance(text_to_clean, string):
            for i in text_to_clean:
                final_text += str(i) + " "
        return final_text

    def evaluate_score(self):
        """
        Instantiates an Evaluator for the method list and results list.
        Evaluator is a valid process only for corpus data.
        """
        g.logger.info("evaluate_score")
        self.evaluator = Evaluator(self.method_list, self.score_list,
                                   self.get_data_list_item(g.DATA_TYPE.TWITTER_CORPUS), '', self.tweet_obj_list)
        self.evaluator.get_methods_evaluated()
        for each in self.method_list:
            g.logger.info("RESULTS FOR METHOD:::"+str(each.name)+": score accuracy"+str(each.accuracy)
                          +" combined accuracy:" + str(each.combined_accuracy))
            print each.name, each.accuracy, each.combined_accuracy

    def start_processing(self, data):
        i = 0
        self.score_list = []
        if isinstance(data, list):
            g.logger.info("start_processing::: list\t"+ str(datetime.datetime.now()))
            for row in data:
                i += 1
                try:
                    g.logger.info("start processing text:\t{0}\t@ {1}".format(row.tweet_text, datetime.datetime.now()))
                except UnicodeError:
                    pass
                ta = self.get_text_analyser(row)
                score_text = (ta.process_text())
                g.logger.info("end processing text:"+str(datetime.datetime.now()))
                g.logger.info(g.log_separator.format(score_text))
                if ta.tweet.lang == 'en':  # to know latter on in the evaluator if a tweet is english or not
                    self.tweet_obj_list.append(ta.tweet.lang)
                    #this is the text evaluation of tweet
                else:
                    self.tweet_obj_list.append('')
                self.score_list.append(ta.sentiment_analyser.scores_list)

        elif isinstance(data, str):
            g.logger.info("start_processing::: str:::\t"+data+'\t'+str(datetime.datetime.now()))
            ta = self.get_text_analyser(data),
            if ta[0].tweet.lang == 'en':  # to know latter on in the evaluator if a tweet is english or not
                self.tweet_obj_list.append(ta[0].tweet.lang)
                #this is the text evaluation of tweet
                score_text = (ta[0].process_text())
                g.logger.info("end processing text:"+ str(datetime.datetime.now()))
            else:
                self.tweet_obj_list.append('')
            self.score_list.append(ta[0].sentiment_analyser.scores_list)
            self.print_results(ta[0])
        else:
            raise TypeError

        return self.score_list

    def save_results(self):
        for m in self.method_list:
            self._conn.update(g.update_calculation_method_where_id.
                                format(m.accuracy, m.combined_accuracy, m.aspect_accuracy,
                                       m.aspect_score_accuracy, m.date_last_evaluated, m.id))

        for s in self.score_list:
            for score in s:
                try:
                    result = self._conn.execute_query(g.select_from_score_where_tweetid.
                                                      format(score.twitter_corpus_id, score.calculation_method_id))
                    g.logger.debug("Result of query: {0}".format(result))
                    if result!=[]:  # we have matching results, go on and perform update
                        self._conn.update(g.update_score_where_twitter_and_method_id.
                                          format(score.score, score.combined_score, score.emoticons_score, score.aspects_list,
                                                score.aspects_score_list, score.date_updated, self.corpus_table_id,
                                                score.processed_tweet, score.twitter_corpus_id,
                                                score.calculation_method_id))
                    else:  # no matching results, go on and insert
                        g.logger.debug("No score found in db for TwitterId {0} and CalculationMethodId={1}, insert results."
                                    .format(score.twitter_corpus_id, score.calculation_method_id))
                        self._conn.update(g.insert_into_score.
                         format(score.twitter_corpus_id, score.calculation_method_id, score.score,
                                score.combined_score, score.emoticons_score, score.aspects_list,
                                score.aspects_score_list, score.date_updated, score.processed_tweet,
                                self.corpus_table_id))

                except:
                    traceback.print_exc()
                    g.logger.debug("No score found in db for TwitterId {0} and CalculationMethodId={1}, insert results."
                                    .format(score.twitter_corpus_id, score.calculation_method_id))
                    self._conn.update(g.insert_into_score.
                                             format(score.twitter_corpus_id, score.calculation_method_id, score.score,
                                                    score.combined_score, score.emoticons_score, score.aspects_list,
                                                    score.aspects_score_list, score.date_updated, score.processed_tweet,
                                                    self.corpus_table_id))
                    continue

    def present_results(self):
        """

        :rtype : object
        """
        stats = StatisticsHandler()
        tmp_mid = []
        tmp_mname = []
        tmp_acc = []
        tmp_combi =[]
        tmp_aspect =[]
        tmp_aspect_score = []
        for m in self.method_list:
            tmp_mid.append(m.id)
            tmp_mname.append(m.name)
            tmp_acc.append(m.accuracy)
            tmp_combi.append(m.combined_accuracy)
            tmp_aspect.append(m.aspect_accuracy)
            tmp_aspect_score.append(m.aspect_score_accuracy)
        print tmp_mname, tmp_acc, tmp_combi, tmp_aspect, tmp_aspect_score
        stats.stats_results = [[tmp_mid, tmp_mname], [tmp_acc, tmp_combi, tmp_aspect, tmp_aspect_score]]
        stats.chart_type = g.CHART_TYPE.BAR
        stats.stats_type = g.STATS_TYPE.METHOD_AND_ACCURACY
        stats.process()

    def print_results(self, ta):
        for score in ta.sentiment_analyser.scores_list:
            print ta.calculation_methods_list[(score.calculation_method_id-1)].name
            print score.score, score.emoticons_score, score.combined_score

    def clean_up(self):
        print "Finished:::", datetime.datetime.now()
        self._conn.close_conn()

    def get_methods_list(self):
        #todo: refactoring!!!
        """


        :return: :rtype:
        """
        temp_list = []
        methods = self._conn.execute_query(g.select_calculation_methods)
        for m in methods:
            method = CalculationMethod(m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9])
            temp_list.append(method)
        return temp_list

    def get_corpus_data_by_table_name(self):
        """


        :return: :rtype: :raise NotImplementedError:
        """
        #fixme: the whole process of which corpus to use is a bit quirky. needs improvement
        if self.corpus_table_id == g.TABLE.TwitterCorpus:
            corpus = self._conn.execute_query(g.select_from_twitter_corpus.
                                              format(table=g.TABLE.reverse_mapping[g.TABLE.TwitterCorpus],
                                                     limit=100))
        elif self.corpus_table_id == g.TABLE.ManualTwitterCorpus:
            corpus = self._conn.execute_query(g.select_from_corpus.
                                              format(table=g.TABLE.reverse_mapping[g.TABLE.ManualTwitterCorpus],
                                                     limit=100))
        elif self.corpus_table_id == g.TABLE.TwitterCorpusNoEmoticons:
            corpus = self._conn.execute_query(g.select_from_corpus.
                                              format(table=g.TABLE.reverse_mapping[g.TABLE.TwitterCorpusNoEmoticons],
                                                     limit=1000))
        else:
            raise NotImplementedError

        return corpus

    def get_corpus_object_list(self):
        """


        :return: :rtype:
        """
        temp_list = []
        # corpus = self.get_corpus_data_by_table_name()
        #
        # for c in corpus:
        #     twitter_corpus = TwitterCorpus(id=c[0], tweet_id=c[0], tweet_text=c[1], emotion_text=c[2], initial_aspect=c[3])
        #     temp_list.append(twitter_corpus)
        return temp_list

    def get_things_rolling(self):
        self.clean_data_from_db()
        self.handle_files()
        self.prepare_lists()
        self.prepare_tagging_model()
        #self.start_processing(self.get_data_list_item(g.DATA_TYPE.TWITTER_CORPUS))

    def prepare_tagging_model(self):
        fh.prepare_model()


#sentifeed = SentiFeed(g.DB_TYPE.MYSQL, g.DATA_TYPE.TWITTER_CORPUS, g.DATA_TYPE.SWN,
                      #g.DATA_TYPE.STOP_WORDS, g.DATA_TYPE.EMOTICONS, g.TABLE.TwitterCorpusNoEmoticons, True)
#sentifeed.get_things_rolling()
#sentifeed.start_processing('These are loooovelyyyy dayyys!!! hahaha :) This is a test omgg rofl loool')
#sentifeed.start_processing('hey @')
#sentifeed.start_processing('Sabias que.... RT @MkDirecto Los usuarios del #iPad desayunan y cenan con la #tableta de @Apple: http://t.co/eYXCQQmK')
#sentifeed.start_processing(sentifeed.get_data_list_item(g.DATA_TYPE.TWITTER_CORPUS))  # the type of data to be examined
                                                                                      # is of twitter corpus
                                                                                      # this is combined with the TABLE
#sentifeed.evaluate_score()
#sentifeed.present_results()
#sentifeed.save_results()
#sentifeed.clean_up()