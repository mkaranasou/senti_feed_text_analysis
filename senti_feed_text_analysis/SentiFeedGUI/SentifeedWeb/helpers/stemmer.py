__author__ = 'maria'
__descr__ ='http://stackoverflow.com/questions/10369393/need-a-python-module-for-stemming-of-text-documents'

from nltk import PorterStemmer
print "Porter Stemmer",PorterStemmer().stem_word('complications')

from nltk.stem import LancasterStemmer
print "Lancaster Stemmer",LancasterStemmer().stem('complications')

from nltk.stem import RSLPStemmer
print RSLPStemmer().stem('complications')

#lemmatizer can be used for more accurate results -- making plural to singular for better match
from nltk.stem import WordNetLemmatizer
print WordNetLemmatizer().lemmatize('complications')


