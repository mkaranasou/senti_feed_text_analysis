__author__ = 'maria'
import database
import helpers.globals as g
import matplotlib.pyplot as mpl

'''SELECT id, TweetId, TweetText, TweetTextAfter, HandcodedNum, SumN, CombinedN, NVAR, NVAROthersN,
                                NVARCombN FROM SentiFeed.SentiFeedResults;'''

handcoded_results = []
sum_results = []
sum_combi_results = []
nvar_results = []
nvar_combi_results = []
id = []


#connect
mysql_server_conn = database.MySQLDatabaseConnector()
#retrieve
results = mysql_server_conn.execute_query(g.select_from_twitter_corpus_results)
#close
mysql_server_conn.close_conn()



def calculate_results(row):
    if row == 1.0:
        return row
    elif row >1.0:
        return 2.0
    elif row <1.0:
        return 0.0

def calculate_results_alt(row):
    if row<=1.05 and row>=0.95:
        return 1.0
    elif row >1.05:
        return 2.0
    elif row <0.95:
        return 0.0


for row in results:
    id.append(row[0])
    handcoded_results.append(row[4])
    sum_results.append(calculate_results(row[5]))
    sum_combi_results.append(calculate_results(row[6]))
    nvar_results.append(calculate_results(row[7]))
    nvar_combi_results.append(calculate_results(row[8]))

def calculate_efficiency(list1, list2):
    score_of_success = 0.0
    for i in range(0,list1.__len__()):
        if list1[i] == list2[i]:
            score_of_success+=1
        i+=1
    return score_of_success/list1.__len__()

s = calculate_efficiency(handcoded_results, sum_results)
sc = calculate_efficiency(handcoded_results, sum_combi_results)
n = calculate_efficiency(handcoded_results, nvar_results)
nc = calculate_efficiency(handcoded_results, nvar_combi_results)

print s, sc, n, nc

fig = mpl.figure()
ax1 = fig.add_subplot(5, 1, 1, axisbg='white')
ax1.plot(id, handcoded_results, 'c', linewidth = 2)
ax1.set_title('Handcoded Tweet Emotion Results')

ax2 = fig.add_subplot(5, 1, 2, axisbg='white')
ax2.plot(id, sum_results, 'c', linewidth = 2)
ax2.plot(id, handcoded_results, 'ro')
ax2.set_title('Sum Success={0}%'.format(s*100))

ax3 = fig.add_subplot(5, 1, 3, axisbg='white')
ax3.plot(id, sum_combi_results, 'c', linewidth = 2)
ax3.plot(id, handcoded_results, 'ro')
ax3.set_title('Sum Combi Success={0}%'.format(sc*100))

ax4 = fig.add_subplot(5, 1, 4, axisbg='white')
ax4.plot(id, nvar_results, 'c', linewidth = 2)
ax4.plot(id, handcoded_results, 'ro')
ax4.set_title('NAVR Success={0}%'.format(n*100))

ax5 = fig.add_subplot(5, 1, 5, axisbg='white')
ax5.plot(id, nvar_combi_results, 'c', linewidth = 2)
ax5.plot(id, handcoded_results, 'ro')
ax5.set_title('NAVR Combi Success={0}%'.format(nc*100))

mpl.show()

mpl.plot(id,handcoded_results, 'ro')
mpl.plot(id,sum_results)
mpl.xlabel('# of Tweets')
mpl.ylabel('Emotion Evaluation')
mpl.axis([0, handcoded_results.__len__(), 0, 3])
mpl.show()

#mpl.hist(handcoded_results, 100, normed=1, facecolor='g', alpha=0.75)
#mpl.show()

'''mpl.plot(id,handcoded_results)
mpl.plot(id,handcoded_results)
mpl.plot(id,handcoded_results)'''