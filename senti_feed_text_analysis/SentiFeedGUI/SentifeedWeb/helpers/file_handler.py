import globals as g

__author__ = 'karanaso'

final = ''
N = []
V = []
A = []
R = []
n_v_a_r_lists = []
pos_book = []
neg_book = []
neu_book = []
train_model = {}
tags = []


def split_sentiwordnet_into_files():
    """


    """
    f = open('../data/swn_corpus/SentiWordNet_3.0.0_20130122.txt', 'r')
    a = open('../data/swn_corpus/adjectives.txt','w')
    n = open('../data/swn_corpus/nouns.txt','w')
    v = open('../data/swn_corpus/verbs.txt','w')
    r = open('../data/swn_corpus/adverbs.txt','w')
    for line in f.readlines():
        list = line.split('\t')
        synsets = list[4].split(' ')
        for i in range(0, len(synsets)):
            score = 1.0 + float(list[2]) - float(list[3])
            final = str(list[0])+ '\t' +str(list[1])+'\t' + str(list[2])+'\t' + str(list[3])+'\t' + str(score)+'\t'+str(synsets[i].replace('_',' '))+'\t'+ str(list[5])
            if list[0]=='r':
                r.write(final)
            elif list[0] == 'a':
                a.write(final)
            elif list[0] == 'n':
                n.write(final)
            elif list[0] == 'v':
                v.write(final)
            else:
                    pass
            #swn_final.txt.write(final)

    f.close()
    a.close()
    n.close()
    v.close()
    r.close()
    #swn_final.txt.close()


#TODO REFACTORING
def handle_files():
    global N,V, A, R, nvar_lists, pos_book, neg_book

    a = open(g.STATIC_PATH_TO_DATA+'/swn_corpus/adjectives.txt','r')
    n = open(g.STATIC_PATH_TO_DATA+'/swn_corpus/nouns.txt','r')
    v = open(g.STATIC_PATH_TO_DATA+'/swn_corpus/verbs.txt','r')
    r = open(g.STATIC_PATH_TO_DATA+'/swn_corpus/adverbs.txt','r')
    #pb = open('../data/book_reviews/positive.review','r')
    #nb = open('../data/reviews/book_reviews/negative.review','r')
    #neu = open('../data/book_reviews/unlabeled.review','r')

    '''for line in pb.readlines():
        pos_book.append(line)'''

    '''for line in nb.readlines():
        neg_book.append(line)'''

    '''NOUNS'''
    temp_list = []
    for line in n.readlines():
        temp_list.append(line.split('\t'))
    for i in range(0, temp_list.__len__()):
        test = str(temp_list[i][5]).split("#")
        N.append((temp_list[i][4], str(test[0])))
    '''VERBS'''
    temp_list = []
    for line in v.readlines():
        temp_list.append(line.split('\t'))
    for i in range(0, temp_list.__len__()):
        test = str(temp_list[i][5]).split("#")
        V.append((temp_list[i][4], str(test[0])))

    '''ADJECTIVES'''
    temp_list = []
    for line in a.readlines():
        temp_list.append(line.split('\t'))
    for i in range(0, temp_list.__len__()):
        test = str(temp_list[i][5]).split("#")
        A.append((temp_list[i][4], str(test[0])))

    '''ADVERBS'''
    temp_list = []
    for line in r.readlines():
        temp_list.append(line.split('\t'))
    for i in range(0, temp_list.__len__()):
        test = str(temp_list[i][5]).split("#")
        R.append((temp_list[i][4], str(test[0])))

    n.close()
    v.close()
    a.close()
    r.close()


def prepare_line():
    o_line = ''
    for i in range(0,145):
        o_line += "="
    return o_line


def prepare_enum_lists():
    global n_v_a_r_lists
    new_list_N = list(enumerate(N))
    new_list_V = list(enumerate(V))
    new_list_A = list(enumerate(A))
    new_list_R = list(enumerate(R))

    n_v_a_r_lists =[new_list_N, new_list_V, new_list_A, new_list_R]

def prepare_gate_twitter_model():
    file_model = open(g.STATIC_PATH_TO_DATA +'twitter_bootstrap_corpus/gate_twitter_bootstrap_corpus.1543K.tokens', 'r')
    out_file_model =open(g.STATIC_PATH_TO_DATA+'twitter_pos/model.txt', 'w')
    tags_file = open(g.STATIC_PATH_TO_DATA + 'twitter_pos/tags.txt', 'w')
    global train_model, tags
    for line in file_model.readlines():
        split_line = line.strip('\n').split(' ')
        for each in split_line:
            tokens = each.split('_')
            if len(tokens) > 2:
                finaltoken =''
                for t in range(0, len(tokens)):
                    finaltoken+= str(tokens[t]) + ' '
                train_model[finaltoken] = tokens[-1]
                out_file_model.write(finaltoken+'\t'+tokens[-1]+'\n')

                if tokens[-1] not in tags:
                    tags.append(tokens[-1])
            else:
                train_model[tokens[0]] = tokens [1]
                if tokens[1] not in tags:
                    tags.append(tokens[1])
                out_file_model.write(tokens[0]+'\t'+tokens[1]+'\n')

    for tag in tags:
        tags_file.write(tag+'\n')

    tags_file.close()
    file_model.close()
    out_file_model.close()

def prepare_model():
    global train_model
    model_file = open(g.STATIC_PATH_TO_DATA+'twitter_pos/model.txt', 'r')
    for line in model_file.readlines():
        split_line = line.strip('\n').split('\t')
        train_model[split_line[0]] = split_line[1]
    model_file.close()


def calculate_polarity(polarity):
    score = int(polarity)
    if score == 0 :
        return 0.0, 'negative'
    elif score == 2:
        return 1.0, 'neutral'
    elif score == 4:
        return 2.0, 'positive'
    else:
        print polarity
        raise ValueError


def save_corpus(processed, table):
    conn = database.MySQLDatabaseConnector()
    for each in processed:
        '''g.logger.debug(g.insert_into_twitter_corpus.\
                    format(each[0], each[1], each[2], each[3],
                           each[4], each[5], each[6], table=table))'''
        conn.update(g.insert_into_twitter_corpus.
                    format(each[0], re.escape(each[1]), each[2], each[3],
                           each[4], each[5], each[6], table=table))



def read_test_data_into_db():
    """
    Where is the tweet corpus?

    Our corpus data can be found here:
    http://cs.stanford.edu/people/alecmgo/trainingandtestdata.zip
    or at this mirror site:
    https://docs.google.com/file/d/0B04GJPshIjmPRnZManQwWEdTZjg/edit

    The data has been processed so that the emoticons are stripped off. Also, it's in a regular CSV format.

    Format
    Data file format has 6 fields:
    0 - the polarity of the tweet (0 = negative, 2 = neutral, 4 = positive)
    1 - the id of the tweet (2087)
    2 - the date of the tweet (Sat May 16 23:58:44 UTC 2009)
    3 - the query (lyx). If there is no query, then this value is NO_QUERY.
    4 - the user that tweeted (robotickilldozr)
    5 - the text of the tweet (Lyx is cool)
    """
    tables = ['ManualTwitterCorpus', 'TwitterCorpusNoEmoticons']
    headers = ['polarity', 'id', 'date', 'query', 'user', 'text']
    ordered_headers = ['id', 'text', 'date', 'query', 'user', 'emotion_num', 'emotion_text']

    manual_test_data_file = '../data/trainingandtestdata/testdata.manual.2009.06.14.csv'
    processed_data_file = '../data/trainingandtestdata/training.1600000.processed.noemoticon.csv'
    csvs = [manual_test_data_file, processed_data_file]
    for each in csvs:
        processed = []
        f = open(each, 'r')
        for line in f.readlines():

            split_l = line.replace('"','').strip('\r\n').split(',')

            tweet_id = split_l[headers.index('id')]
            tweet_text = split_l[headers.index('text')]
            query = split_l[headers.index('query')]
            create_date = split_l[headers.index('date')]
            user = split_l[headers.index('user')]
            emotion_num, emotion_text = calculate_polarity(split_l[headers.index('polarity')])

            processed.append([tweet_id, tweet_text, create_date, query, user, emotion_num, emotion_text])
        save_corpus(processed, tables[csvs.index(each)])