import re

__author__ = 'maria'

word_list = ['juuusttt', 'a', 'testyy', 'looooveee', 'tweet']
'''counter = 0
duplicates_in_word = 0

for word in list:
    counter = 0
    duplicates_in_word = 0
    final_word = ''
    print "initial word = "+word
    for i in range(0, word.__len__()-1):  #for each character in this word
        print i, word[i]

        if word[i] == word[i+1]:#if the i-th character is equal with the previous
            if counter <= 2:# and no duplicates till now
                print word[i], word[i+1]
                final_word +=word[i+1] #then it's ok to keep all characters till now
            if (i == word.__len__()-2):
                if word[i]== word.__len__()-1:
                    print "blah",i
                    final_word += word[i+1]
        else:
            final_word += word[i]
            counter += 1                #increase duplicate counter

    print final_word
'''
'''        else:                           # else if i-th character is not equal to the previous
            if counter > 1 :            #check if counter indicates that before i there were more than 2 letters in row
                print "duplicate indicator, do sth"
                print "dupl counter:", duplicates_in_word
                duplicates_in_word+=1   # this counter indicates how many unique duplicates in word, e.g. in juuuustttt there are two 'u' and 't'

                if duplicates_in_word >1:# if it is the second unique
                    final_word = word[i-counter-2:]
                    print "dupl >1"
                else:
                    for i in range(counter-2, counter+1):
                        final_word = word[:i-counter+1] + word[counter+1:]
                    print "dupl <1 "

                print "dupl counter:", duplicates_in_word
            else:
                final_word = word
            counter = 0                 #reset counter and go for the next letter in word'''

def remove_multiples():
    """
    assume that two characters in a row are valid. If not they should be corrected in spell-checking
    :return:
    """
    counter = 0
    final_word = ''
    print word_list.__len__()
    #for l in range(0, word_list.__len__()):
    for word in word_list:
        for i in range(1, word.__len__()):
            if word[i - 1] == word[i]:
                counter += 1
            else:
                if counter > 2:
                    print "duplicate indicator, do sth"
                    for j in range(counter - 2, counter + 1):
                        print word[:j - counter + 1] + word[counter + 1:-1]
                        final_word += word[:j - counter + 1] + word[counter + 1:-1]
                        print final_word
                counter = 0


def remove_multiples_v2(word_list):
    for word in word_list:
        multi_counter = 0
        index_list = []
        l_word = list(word)
        if l_word.__len__() > 2:  # It doesn' t make sense to look for multiples in words with less than 3 characters
            for i in range(0, l_word.__len__()):
                try:
                    # if this letter equals the next letter then increment multi_counter
                    if l_word[i] == l_word[i+1]:
                        multi_counter += 1
                    else:
                        multi_counter = 0

                except IndexError:
                    if l_word[i] == l_word[i-1] and l_word[i] == l_word[i-2]:
                        multi_counter += 1
                    continue
                try:
                    # if more than 2 duplicates, keep the index of letter to be removed
                    if multi_counter > 1:
                        print 'multiples {0} {1}'.format(l_word[i+1], i+1)
                        index_list.append(i+1)
                except IndexError:
                    if l_word[i] == l_word[i-1] and multi_counter > 1:
                        index_list.append(i)
                    pass
        # in order to avoid index errors, start removing from the end of the word
        reverse_indexes = sorted(index_list, reverse=True)
        for each in reverse_indexes:
            l_word.remove(l_word[each])

        final_word = ''.join(l_word)
        print final_word

def removeRT(text):
    rt = re.findall('^RT\s{0,1}(@\w+:){0,1}', text)
    if rt!=[]:
        print rt
        text = re.sub('^RT:{0,1}', '', text)
        print(text)

#removeRT('RT: @someone:test etbwAESJBF GRDFG NNNnubga ret rt')

def laughter(text):
    found_haha = re.findall(r'((o(m)*(g)*)|o-m-g|o\sm\sg)', text)
    print found_haha

laughter('o m g o-m-g')