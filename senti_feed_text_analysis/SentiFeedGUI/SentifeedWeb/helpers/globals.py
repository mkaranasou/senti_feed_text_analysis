import datetime

__author__ = 'maria'

import logging
import inspect
import os
import platform

STATIC_PATH_TO_DATA_nix = '/Users/maria/senti_feed_text_analysis_repo/senti_feed_text_analysis/SentiFeedGUI/' \
                          'SentifeedWeb/static/data/'
STATIC_PATH_TO_DATA_win = 'C:\\Users\\M\\Desktop\\senti_feed_text_analysis_project\\senti_feed_text_analysis\\' \
                          'SentiFeedGUI\\SentifeedWeb\\static\\data\\'

if platform.system() == 'Windows':
    STATIC_PATH_TO_DATA = STATIC_PATH_TO_DATA_win
else:
    STATIC_PATH_TO_DATA = STATIC_PATH_TO_DATA_nix

logger = None
ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
PROJECT_ROOT_PATH = str(ROOT_PATH).replace('helpers','')
TWITTIE_JAR_PATH = PROJECT_ROOT_PATH + '/data/twitie-tagger/twitie_tag.jar'
TWITTIE_MODEL_PATH = PROJECT_ROOT_PATH + '/data/twitie-tagger/models/gate-EN-twitter.model'
DATESTAMP = str(datetime.datetime.today()).replace(' ', '_').replace(':', '_')
log_separator = "#################################{0}####################################################"

###################APPLICATION KEYS - TODO STORE THEM ELSEWHERE #############################
ckey = 'UmHSnVjiOnI8uUOQQtLQqQ'
csecret = 'CJxZ7790yM3bJgyymgLFzXyimugjY6b820xCMW8xS0U'
atoken = '85281300-55pVbdrlhWTPSNq5dLj2z8oR0zRcX4kdmRVOfkEei'
asecret = 'j9SPfNbCOdLQpZzcBjpJHMgFGTibb9AD7rRf4gCgV1cHF'
#############################################################################################
################################################ LOGGER ################################################################
LOG_PATH = PROJECT_ROOT_PATH + "/logs/sentifeed{0}.log"\
                                .format(DATESTAMP)
def init_logger():
    global logger
    logger = logging.getLogger('baulogger')
    handler = logging.FileHandler(LOG_PATH)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

def autolog(message):
    """
    Automatically log the current function details.
    source:
    http://stackoverflow.com/questions/10973362/python-logging-function-name-file-name-line-number-using-a-single-file
    """
    global logger
    # Get the previous frame in the stack, otherwise it would
    # be this function!!!
    func = inspect.currentframe().f_back.f_code
    # Dump the message + the name of this function to the log.
    logger.debug("%s: %s in %s:%i" % (
        message,
        func.co_name,
        func.co_filename,
        func.co_firstlineno
    ))

init_logger()
################################################ LOGGER ################################################################
################################################ ENUMS ################################################################

def enum(*sequential, **named):
    """
    REFERENCE = http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python
    """
    enums = dict(zip(sequential, range(len(sequential))), **named)
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['reverse_mapping'] = reverse
    return type('Enum', (), enums)

######################################## Globals for Database Handling #################################################
DB_TYPE = enum(MYSQL=0, MSSQL=1, MONGODB=2)
DATA_TYPE = enum(TWITTER_CORPUS=0, SWN=1, STOP_WORDS=2, EMOTICONS=3)
TABLE = enum(TwitterCorpus=0, TwitterCorpusV2=1, ManualTwitterCorpus=2, TwitterCorpusNoEmoticons=3)
SOURCE = enum(Corpus=0, Text=1, Stream=2)
######################################## Globals for Evaluation of results ############################################
METHOD_TYPE = enum(GENERAL=0, ASPECT_BASED=1, GEO_BASED=2)
######################################## Globals for Statistics ########################################################
STATS_TYPE = enum(TEXT_AND_SCORE=0, METHOD_AND_ACCURACY=1 )
CHART_TYPE = enum(BAR=0, PIE=1)
######################################## Globals for Aspect Analysis ###################################################
WORD_TYPE = enum(NOUN=0, VERB=1, ADJECTIVE=2, ADVERB=3)
################################################ ENUMS ################################################################
######################################## Globals for Text Processing and Sentiment Analysis ############################
f = open(STATIC_PATH_TO_DATA + '/swn_corpus/SentiWordNet_3.0.0_20130122.txt', 'r')
a = open(STATIC_PATH_TO_DATA + '/swn_corpus/adjectives.txt', 'a')
n = open(STATIC_PATH_TO_DATA + '/swn_corpus/nouns.txt', 'a')
v = open(STATIC_PATH_TO_DATA+ '/swn_corpus/verbs.txt', 'a')
r = open(STATIC_PATH_TO_DATA+ '/swn_corpus/adverbs.txt', 'a')
final = ''
N = []
V = []
A = []
R = []

NOUNS = ['N', 'NP', 'NN', 'NNS', 'NNP', 'NNPS']
VERBS = ['V', 'VD', 'VG', 'VN', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
ADJECTIVES = ['ADJ', 'JJ', 'JJR', 'JJS']
ADVERBS = ['RB', 'RBR', 'RBS', 'WRB']
SMILEY_FULL_PATTERN = '(:-\)|:-\(|:-\(\(|:-D|:-\)\)|:\)\)|\(:|:\(|:\)|:D|=\)|;-\)|XD|=D|:o|' \
                      ':O|=]|D:|;D|:]|=/|=\(!:\'\(|:\'-\(|:\\|:/|:S|<3)'
NEGATIONS_PATTERN = "(no|not|isnt|isn\'t|isn;t|don\'t|won\'t|couldn\'t|can\'t|didn\'t|not|didnt|didn;t|dont|don;t|" \
                    "don t|won t|can;t|cant|can t|doesn\'t|doesnt|doesn t|doesn;t|cannot|did not)"
CAPITALS_PATTERN = '[A-Z]'
#LAUGHING_PATTERN = "(?>a*+(?>ha)++h?|(?>l++o++)++l++|haha*|rol(t?)f|lo*l|lmao)"
__source__ = 'http://stackoverflow.com/questions/3895874/regex-to-match-lol-to-lolllll-and-omg-to-omggg-etc'
LAUGHING_PATTERN_haha = '\b(a?((h)*(a)*)*)\b'
'''LAUGHING_PATTERN_lol = """(?ix)\b    # assert position at a word boundary
    (?=l(o)*(l)*)        # assert that "lol" can be matched here
    \S*            # match any number of characters except whitespace
    (\S+)          # match at least one character (to be repeated later)
    (?<=\blo)     # until we have reached exactly the position after the 1st "lol"
    \1*            # then repeat the preceding character(s) any number of times
    \b             # and ensure that we end up at another word boundary"""'''
LAUGHING_PATTERN_lol= '(?ix)\b(l(o)*(l)*)\1*\b'
LAUGHING_PATTERN_rotfl = '(?ix)\b(?=ro(t)*fl)'
LAUGHING_PATTERN_omg = '(?ix)\b(?=o(m)*(g)*)'

NEGATIONS = ['no', 'don\'t', 'won\'t', 'couldn\'t', 'can\'t', 'didn\'t', 'not', 'could not', 'cannot']
######################################## Globals for Text Processing and Sentiment Analysis ############################

######################################## For table SentiFeedResults ####################################################
insert_results_query = '''INSERT INTO SentiFeed.SentiFeedResults(TweetId,TweetText,TweetTextAfter, HandcodedT,HandcodedNum,
                        SumN,EmoticonsN,CombinedN,CombinedT,NVAR,NVAROthersN, NVARCombN,Lang,EmoticonsT,
                        SpellingCorrection,Aspect,ReferencesT,Hashlist, HashlistProcessed ) VALUES
                        ('{0}', "{1}", '{2}','{3}', '{4}', '{5}', '{6}', '{7}','{8}', '{9}', '{10}'
                            , '{11}', "{12}", "{13}","{14}","{15}","{16}","{17}", "{18}");'''
update_results_query = '''UPDATE SentiFeed.SentiFeedResults SET TweetText = "{1}",
                       TweetTextAfter = '{2}', HandcodedT = '{3}', HandcodedNum = '{4}',
                       SumN = '{5}', EmoticonsN = '{6}', CombinedN = '{7}', CombinedT = '{8}',
                       NVAR = '{9}', NVAROthersN = '{10}',NVARCombN = '{11}',Lang = "{12}",
                       EmoticonsT = "{13}", SpellingCorrection = "{14}",
                       Aspect = "{15}",ReferencesT = "{16}",Hashlist = "{17}", HashlistProcessed = "{18}"
                       WHERE TweetId = '{0}';'''

########################################################################################################################

######################################## For retrieving basic data #####################################################
select_from_twitter_corpus = "SELECT id, TweetText, HandcodedEmotion, Aspect FROM SentiFeed.{table} limit {limit}"
select_from_corpus = "SELECT TweetId, TweetText, EmotionT, Query FROM SentiFeed.{table} limit {limit};"
select_from_swn = "SELECT Category, SysTerms, SentimentAssesment FROM SentiFeed.SentiWordNetTemp"
select_from_stop_words = "SELECT StopWord FROM SentiFeed.StopWordsSample"
select_from_top_emoticons = "SELECT Emoticon, PersonalEvaluationScore FROM Top20Emoticons"
sum_query = '''SELECT sum(SentimentAssesment), count(SentimentAssesment) FROM SentiFeed.SentiWordNetTemp
                where SysTerms like '%{0}%#%';'''

select_calculation_methods = "SELECT * FROM SentiFeed.CalculationMethod;"

select_from_corpusv2_where_emotion = '''SELECT * FROM SentiFeed.TwitterCorpusV2 where HandcodedT = '{0}';'''
select_all_from_source = '''SELECT * FROM sentifeed.source;'''
########################################################################################################################
###################################### For Updating Corpus #############################################################
update_twitter_corpus_q = "UPDATE TwitterCorpus set TextEmotion = '{0}', ProcessedTweet = '{1}', Emoticons ='{2}', " \
                          "TextEmotionEval ='{3}', CombinedScore = '{4}', EmoticonEmotionEval = '{5}'," \
                          " NVARScore = '{6}', NVARCombi = '{7}'  where id = '{8}'"
update_twitter_corpusv2_q = "UPDATE TwitterCorpusV2 set TextEmotion = '{0}', ProcessedTweet = '{1}', Emoticons ='{2}', " \
                          "TextEmotionEval ='{3}', CombinedScore = '{4}', EmoticonEmotionEval = '{5}'," \
                          " NVARScore = '{6}', NVARCombi = '{7}'  where id = '{8}'"


#################################################### For statistics ####################################################
select_from_sentifeedresults = '''SELECT id, TweetId, TweetText, TweetTextAfter, HandcodedNum, SumN, CombinedN, NVAR,
                                NVAROthersN,NVARCombN FROM SentiFeed.SentiFeedResults limit 100;'''
select_from_twitter_corpus_results = '''SELECT id, id, TweetText, ProcessedTweet, HandcodedEmotion, TextEmotionEval,
                                        CombinedScore, NVARScore, EmoticonEmotionEval,
                                        NVARCombi FROM SentiFeed.TwitterCorpus limit 100;'''
update_calculation_method_where_id = '''UPDATE SentiFeed.CalculationMethod
                                        SET
                                        Accuracy = '{0}',
                                        CombinedAccuracy = '{1}',
                                        AspectAccuracy = '{2}',
                                        AspectScoreAccuracy = '{3}',
                                        DateLastEvaluated = '{4}'
                                        WHERE id = '{5}';'''

update_score_where_twitter_and_method_id = '''UPDATE SentiFeed.Score
                                        SET
                                        Score = "{0}",
                                        CombinedScore = "{1}",
                                        EmoticonsScore = "{2}",
                                        AspectsList = "{3}",
                                        AspectsScoreList = "{4}",
                                        DateUpdated = "{5}",
                                        Source = "{6}",
                                        ProcessedTweet = "{7}"
                                        WHERE TwitterCorpusId = "{8}" AND MethodId = "{9}";
                                        '''

insert_into_score = '''	INSERT INTO SentiFeed.Score
         ( TwitterCorpusId, MethodId, Score, CombinedScore,
         EmoticonsScore, AspectsList,AspectsScoreList, DateUpdated, Source, ProcessedTweet )
          VALUES
         ( "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}", "{8}", "{9}");
                    '''

insert_into_twitter_corpus = '''INSERT INTO SentiFeed.{table}
                              (TweetId, TweetText, CreateDate, Query, User, EmotionN, EmotionT)
                              VALUES
                              ("{0}", "{1}", "{2}", "{3}","{4}", "{5}", "{6}");
                                        '''
select_from_score_where_tweetid = '''SELECT * FROM SentiFeed.Score where TwitterCorpusId = '{0}' and MethodId = '{1}';'''

############################################ For Aspect Analysis #######################################################
select_from_corpusv2_where_aspect = ''''''

select_from_corpusv2_where_aspect_emotion = '''SELECT * FROM SentiFeed.TwitterCorpusV2 where HandcodedT = 'positive'
                                                and InitialAspect = 'apple';'''
############################################ For Aspect Analysis #######################################################
#################################################### Reviews ###########################################################
positive_book_review = '''holes must top_secret he center other_civilans the_pacific the_navy a_lot surface_must this_book
man_named <num>_feet would_strongly put_down norman_johnson lawes a_top the_support ten on_random typhoon a_phycologist
pressure actually_an a_day johnson_is strange civilans_to explored support pacific_ocean
pressure_to back_some read however_on it_still stuck a_remote american find_out not_have other_crichton analysis.i black_holes
after_a misssion some_strange half_mile <num>_navy actually remote_location the_story behavior civilans research_that
michael_crichton excellant_novel strongly_recommend some michael does_not strange_things sphere_by a_man are_joined mile_long
put operations spacecraft around joined an_american sea a_half pacific the_civilans information_on day_under half that_some
lot around_a travels ten_is excellant however novels find brought random one the_ocean team a_typhoon read_the all_of
feet_under by_michael by_<num> out_that crichton_novels support_ships the_other sea_they strongly has_brought earth.this
he_travels quickly novels_have things_back ocean_is personel recommend remote leave explored_black a_center run_operations
certainly_the is_stuck have_read random_things center_<num> of_all out best help still novel they_are the_lawes feet
phycologist_he day down phycologist analysis.i_would they_quickly help_the <num>_other spaceship hardest_to lot_of one_of
help_them spacecraft_they comes back_to holes_and researching_the lawes_of crichton_is stuck_<num> to_help crichton to_earth.this
navy_in behavior_analysis.i researching book they learn operations_however with_<num> them to_put typhoon_comes the_spacecraft was
live_while surface_of partial still_has <num> navy spaceship_the ships the_crichton named norman of_information american_ship man
spacecraft_is top ocean_after revolves_around all other of_partial team_of novels_that must_leave leave_the live i_have
the_team mile story travels_with location earth.this_novel novel_does johnson johnson_johnson that_i ship some_of surface
brought_back them_run while of_ten secret long has_explored is_actually to_live was_certainly the_surface to_behavior
learn_that named_norman run quickly_learn this_was i the_hardest the_best ships_on research travel_to long_spaceship
an_excellant joined_by secret_misssion the_sea certainly personel_to surface_a while_researching not revolves they_find
hardest location_in back navy_personel story_revolves ocean things_from partial_pressure best_crichton comes_and civilans_travel
misssion_they recommend_this sphere the_research black down_of things travel ocean_to ship_that after information novel_this'''

negative_book_review = '''avid your horrible_book wasted use_it the_entire money.i i_lit i_read lit i_would relationship read a_<num> reader_and
reader suffering fire_one i_had year_old gotten horrible lit_this world...don't my one_star headache_the this_book mom
was_horrible friend book_horrible star_i back avid_reader than_one life copy rate_it rate my_mom man book_was half
on_fire and_then reading_this so lower i_could <num>_year than time half_of time_spent then book and_picked possible
spent old_man up_after one horrible_if one_less part was entire less_copy to_rate my_life about_the your_money.i
an_avid if the_relationship use a_headache fire lower_than reading a_friend picked purposes then_got waste_your after_my
friend_i old man_and and_i world...don't_waste book_on part_about copy_in book_back book_wasted have_i time_and the_world...
don't better if_it star got mom_had read_half waste after i about could_use had_gotten was_possible year it_lower
relationship_the wasted_my wish wish_i boy purposes_this got_to the_time it_was back_so suffering_from spent_reading book_up
less better_purposes headache possible_to money.i_wish for_better it_suffering the_part gotten_it picked_this entire_time
old_boy i_am the_<num> boy_had <num> so_i'''

positive_apparel_review = '''The quality is much better than expected. I bought one for myself and one for my husband
prior to our Las Vegas trip Sept. 6, 2005. We were able to walk with or hands free because these fannypacks
held all of the necessities, our cell phones, travelers checks, keys, and medicines in particular.
We love the spacious divided compartments. I highly reccommend this product..
'''
negative_apparel_review = '''
I have to say that I was disappointed when I opened up the package containing my iPod wallet.
It's cute, but not $60ish cute. First of all, it's not leather, it's nylon.
The lining is indeed suede, but the photos in the product listing are misleading.
I'm keeping it because the hassle of shipping it back, etc. isn't worth it.
It does the job, but it wasn't what I was expecting. I feel ripped off
'''

pos_book_review_insert = '''INSERT INTO SentiFeed.BookReviewsPos (Review, CleanReview, EvaluationT, SumN, NVARN, NVARCombiN)
VALUES ("{0}", '{1}', '{2}', '{3}', '{4}', '{5}');'''
neu_book_review_insert = '''INSERT INTO SentiFeed.BookReviewsNeu (Review, CleanReview, EvaluationT, SumN, NVARN, NVARCombiN)
VALUES ("{0}", '{1}', '{2}', '{3}', '{4}', '{5}');'''
neg_book_review_insert = '''INSERT INTO SentiFeed.BookReviewsNeg (Review, CleanReview, EvaluationT, SumN, NVARN, NVARCombiN)
VALUES ("{0}", '{1}', '{2}', '{3}', '{4}', '{5}');'''

ins_ApparelReviewUncat = '''INSERT INTO SentiFeed.ApparelReviewUncat (Review, CleanReview, EvaluationT, SumN, NVARN, NVARCombiN)
VALUES ("{0}", '{1}', '{2}', '{3}', '{4}', '{5}');'''

ins_ApparelReviewPos = '''INSERT INTO SentiFeed.ApparelReviewPos (Review, CleanReview, EvaluationT, SumN, NVARN, NVARCombiN)
VALUES ("{0}", '{1}', '{2}', '{3}', '{4}', '{5}');'''

ins_ApparelReviewNeg = '''INSERT INTO SentiFeed.ApparelReviewNeg (Review, CleanReview, EvaluationT, SumN, NVARN, NVARCombiN)
VALUES ("{0}", '{1}', '{2}', '{3}', '{4}', '{5}');'''
'''<review_text>'''

#http://www.laurentluce.com/posts/twitter-sentiment-analysis-using-python-and-nltk/
pos_tweets = [('1', 'I love this car', 'positive'),

              ('2', 'This view is amazing', 'positive'),

              ('3','I feel great this morning', 'positive'),

              ('4', 'I am so excited about the concert', 'positive'),

              ('5', 'He is my best friend', 'positive')]

neg_tweets = [('6', 'I do not like this car', 'negative'),

              ('7','This view is horrible', 'negative'),

              ('8','I feel tired this morning', 'negative'),

              ('9', 'I am not looking forward to the concert', 'negative'),

              ('10', 'He is my enemy', 'negative')]
