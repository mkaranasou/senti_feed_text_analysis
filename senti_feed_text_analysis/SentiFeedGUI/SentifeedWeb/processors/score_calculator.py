
__author__ = 'maria'

from senti_feed_text_analysis.helpers.globals import NOUNS, VERBS, ADJECTIVES, ADVERBS
import senti_feed_text_analysis.helpers.file_handler as fh


class ScoreCalculator(object):
    def __init__(self):
        pass


    def get_n_v_a_r_from_pos_tag(self, pos_tagger):
        """
        store nouns, verbs, adjectives, adverbs from pos tag in lists in order to check for score in respective files
        :param pos_tagger: pos tagging results from different taggers
        :return:
        """
        '''self.nouns =[]
        self.verbs = []
        self.adjectives = []
        self.adverbs = []
        self.aspect = []'''
        if type(pos_tagger) == list:  # in the case of gate tagger
            for word, tag in pos_tagger:
                if tag in NOUNS:
                    self.nouns_g.append(word)
                    self.aspect_g.append(word)  # moved from get_aspect to simplify
                elif tag in VERBS:
                    self.verbs_g.append(word)
                elif tag in ADJECTIVES:
                    self.adjectives_g.append(word)
                elif tag in ADVERBS:
                    self.adverbs_g.append(word)
                else:
                    self.non_nvar_g.append(word)

        else:
            for key in pos_tagger.keys():
                if pos_tagger[key] in NOUNS:
                    self.nouns.append(key)
                    self.aspect.append(key)  # moved from get_aspect to simplify
                elif pos_tagger[key] in VERBS:
                    self.verbs.append(key)
                elif pos_tagger[key] in ADJECTIVES:
                    self.adjectives.append(key)
                elif pos_tagger[key] in ADVERBS:
                    self.adverbs.append(key)
                else:
                    self.non_nvar.append(key)

    def calculate_n_v_a_r_score(self, nouns, verbs, adjectives, adverbs):
        """supposing that pos tagging works well
           we calculate the score from each word in lists
        :param nouns:
        """
        trials = 0
        misses = 0
        hits = 0
        missed = []

        #enumerate the lists in order to be able to get the position when score is found
        enumerated_lists = fh.n_v_a_r_lists
        #n_v_a_r_lists = self.prepare_nvar_lists()
        n_v_a_r_lists = [nouns, verbs, adjectives, adverbs]
        score_list = []
        score = 0.0
        #for the four lists noun, verb, adjective, adverb
        for nvar_list in enumerated_lists:
            for item in n_v_a_r_lists[enumerated_lists.index(nvar_list)]:
                trials += 1
                exists = self.find_if_and_where_in_list(nvar_list, item, 2)
                if exists != None and exists != []:
                    hits += 1
                    score_list.append(nvar_list[exists[0]][1][0])
                else:
                    misses += 1
                    missed.append(item)
        self.print_statistics_of_trials(hits, missed, misses, trials)
        if score_list.__len__() > 0:
            for i in score_list:
                score += float(i)
            self.nvar_score = score / score_list.__len__()
        else:
            #print "no NVAR score"
            self.nvar_score = 1.0
        return self.nvar_score

    def calculate_non_nvar_score(self):
        non_nvar_temp_score = 0.0
        score = self.calculate_non_nvar_sum_score(self.non_nvar)
        if score.__len__() > 0:
            for s in score:
                if not s is None:
                    non_nvar_temp_score += s
            if non_nvar_temp_score != 0.0:  # We do have Score!
                self.non_nvar_score = non_nvar_temp_score / score.__len__()

        return self.non_nvar_score

    def search_db_for_sum_score(self, word, spellchecking=False):
        """

        :param word:
        :param spellchecking:
        :return: :rtype:
        """
        if spellchecking:
            return self.mysql_server_conn.execute_query(
                g.sum_query.format(self.spell_checking(word)))
        else:
            return self.mysql_server_conn.execute_query(g.sum_query.format(word))

    def calculate_non_nvar_sum_score(self, text):
        """
        :param text:
        :return: :rtype:
        """
        hits = 0
        misses = 0
        trials = 0
        missed = []
        score = []

        for word in text:
            if word.__len__() > 2:
                try:
                    trials += 1
                    temp_score = self.search_db_for_sum_score(word, False)
                    if not temp_score[0][0] == None:
                        hits += 1
                        score_per_times = temp_score[0][0] / temp_score[0][1]
                        score.append(score_per_times)
                    #if no score could be found for word, try spell checking -- duplicate code todo: refactor
                    elif self.contains_errors:
                        temp_score = self.search_db_for_sum_score(word, True)
                        if not temp_score[0][0] == None:
                            score_per_times = temp_score[0][0] / temp_score[0][1]
                            score.append(score_per_times)
                    else:
                        misses += 1
                        missed.append(word)
                except:
                    traceback.print_exc()
        self.print_statistics_of_trials(hits, missed, misses, trials)
        return score

    def calculate_text_score(self):
        score = []
        times_found = 0.0
        final_score = 0.0
        temp_score = None
        hits = 0
        misses = 0
        trials = 0
        missed = []

        for word in self.words:
            if word.__len__() > 2 and word not in self.negations:  # take into account only words with more than 2 letters
                trials += 1
                temp_score = self.search_db_for_sum_score(word, False)
                print "W/out SPELLCHEKCKING:", temp_score
                if not None == temp_score[0][0]:
                    score_per_times = temp_score[0][0] / temp_score[0][1]
                    score.append(score_per_times)
                    hits += 1
                #if no score could be found for word, try spell checking -- duplicate code todo: refactor
                elif self.contains_errors and word in self.errors_in_text:
                    try:
                        g.logger.debug("contains errors {0}, try with {1}".format(self.contains_errors, word))
                        temp_score = self.search_db_for_sum_score(word, True)
                        print "WITH SPELLCHEKCKING:", temp_score
                        if not temp_score[0][0] == None:
                            score_per_times = temp_score[0][0] / temp_score[0][1]
                            score.append(score_per_times)
                        g.logger.debug("Spellchecked result = {0}".word)
                    except:
                        score.append(1.0)
                else:
                    misses += 1
                    missed.append(word)
        self.sentence_score = score

                #TODO Too complicated for no reason. Make it cleaner
        if self.sentence_score.__len__() > 0:
            for s in self.sentence_score:
                final_score += s
            self.tweet_score = final_score / self.sentence_score.__len__()
        else:  # NO SCORE
            self.tweet_score = 1.0

        self.print_statistics_of_trials(hits, missed, misses, trials)

        return self.tweet_score

    def calculate_sentiment(self, score):
        """ 0: negative, 1:neutral, 2:positive etc. returns the string of evaluation
        :param score:float
        :return:None
        """
        emotion = ""
        if self.tweet_score < 0 or self.tweet_score == 1.0:
            emotion = "neutral"
        elif 1.0 < self.tweet_score < 1.25:
            emotion = "somewhat positive"
        elif self.tweet_score >= 1.25:
            emotion = "positive"
        elif 1.0 > self.tweet_score > 0.75:
            emotion = "somewhat negative"
        elif 0.0 <= self.tweet_score <= 0.75:
            emotion = "negative"

        g.logger.info("Tweet/Emotion = {0}, {1}".format(self.init_text, emotion))
        return emotion

    def calculate_combined_score(self, score):
        """
        combined means text score and emoticons score
        combined score calculation = [(emoticons score/ num of emoticons) + general method score]/2.0
        :param score:
        :return:
        """
        if self.em.__len__() > 0:  # if there is emoticons score - go on and calculate combi score for any method
            self.combined_score = ((self.emoticons_score / self.em.__len__()) + score) / 2.0
            self.nvar_combi_score = ((self.emoticons_score / self.em.__len__())
                                     + self.nvar_score + self.non_nvar_score) / 3.0
            return ((self.emoticons_score / self.em.__len__()) + score) / 2.0
        else:
            self.combined_score = score
            self.nvar_combi_score = (self.nvar_score + self.non_nvar_score) / 2.0
            return score

    def get_emoticons_score(self):
        """
        Look up every emoticon found in smileys per sentence list. If found, save score in em
        """
        new_list = list(enumerate(self.top20emoticons))
        for each in self.smileys_per_sentence:
            for emoticon in each:
                exists = [n for n, (i, s) in new_list if i == emoticon]
                if exists != None and exists != []:
                    self.em.append(new_list[exists[0]][1][1])

    def calculate_emoticons_score(self):
        if self.em.__len__()>0:
            for s in self.em:
                self.emoticons_score += s
        else:
            self.emoticons_score = 1.0
