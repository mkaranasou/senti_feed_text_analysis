import traceback
import nltk
from SentifeedWeb.helpers import globals as g
from nltk.tag import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tag.simplify import simplify_wsj_tag
from SentifeedWeb.helpers import file_handler as fh

__author__ = 'maria'


class POSTagger(object):
    """
    Handles postagging, stemming, lemmatizing.
    """
    def __init__(self):
        ########### POS Tagging ############
        self.words_pos_tags2 = []
        self.words_pos_tags = {}
        self.simplified = []
        ############ N-grams ###############
        self.trigram_tagger = []
        self.gate_tagger = []
        ############ Stemming ###############
        self.init_stemmers()
        self.stemmed_words = []
        self.porter = []
        self.lancaster = []
        self.regex = []
        self.iris = []
        self.rspl = []
        ############# Lemmatizing ###########
        self.lemmatizer = WordNetLemmatizer()
        self.lemmatized_words = []

    def pos_stem_lematize(self, tweet):
        self.tweet = tweet
        self.tweet_words_in_a_single_list = self.get_tweet_words_in_a_single_list()
        self.apply_lemmatizing()
        self.apply_stemming()
        self.pos_tag_words()
        #self.get_position_of_words_in_sentence()
        self.trigram_pos_tag_words()
        self.custom_pos_tagger()
        self.bigram_pos_tagger()
        self.gate_experimental_pos_tagging_using_model()

        #################################################### POS TAGGING ###################################################
    def pos_tag_words(self):
        """
        Get pos tag for every valid word in each sentence.
        Updates self.words_pos_tags with pos tagged sentences. Result will be like: [[pos tagged sentence 1],[...2]]
        :return:None
        """
        try:
            #fixed bug 2-2-2013
            self.words_pos_tags.update(pos_tag(self.tweet_words_in_a_single_list))  #todo what about when we have more than 1 sentence? to be checked
            self.words_pos_tags2 = pos_tag(self.tweet_words_in_a_single_list)
            self.simplified = [(word, simplify_wsj_tag(tag)) for word, tag in self.words_pos_tags2]
            #print simplified
            g.logger.info("pos_tag_words:::\t"+str(self.words_pos_tags)+'\t'+str(self.words_pos_tags2))
            g.logger.info("pos_tag_words simplified:::\t"+str(self.simplified))
        except:
            traceback.print_exc()
            g.logger.error(("could not pos tag:" + str(self.tweet.words)))
            print "could not pos tag:" + str(self.tweet.words)

    def tree_representation(self):
        tree2 = nltk.tagstr2tree(self.tweet.sentences[0])
        tree2.draw()

    def custom_pos_tagger(self):
        """
        source: http://stackoverflow.com/questions/5919355/custom-tagging-with-nltk
        :return:
        """
        default_tagger = nltk.data.load(nltk.tag._POS_TAGGER)
        model = {'select': 'VB'}
        tagger = nltk.tag.UnigramTagger(model=model, backoff=default_tagger)
        self.custom_tag_results = tagger.tag(self.tweet_words_in_a_single_list)
        g.autolog('custom_pos_tagger :::\t'+str(self.custom_tag_results))

    def trigram_pos_tag_words(self):
        """
            Use a trigram tagger to get better results for aspect analysis.
        """
        tagged_corpora = []
        tweet_words = self.tweet_words_in_a_single_list
        tagged_corpora.append(pos_tag(tweet_words))  #tagged sentences with pos_tag

        if len(tagged_corpora) > 0:
            try:
                g.logger.info(str(tagged_corpora))
                trigram_tagger = nltk.TrigramTagger(tagged_corpora)  #build trigram tagger based on your tagged_corpora
                trigram_tag_results = trigram_tagger.tag(tweet_words)  #tagged sentences with trigram tagger
                for j in range(0, len(tagged_corpora)):
                    if tagged_corpora[j][1] == 'NN':
                        tagged_corpora[j][1] = trigram_tag_results[j][1]  #for 'NN' take trigram_tagger instead

                self.trigram_tagger = tagged_corpora
                g.logger.info("trigrams:::\t"+ str(tagged_corpora))
                g.logger.info("trigram results:::\t"+ str(trigram_tag_results))
            except:
                g.logger.error("problem in trigram.")
                traceback.print_exc()

    def bigram_pos_tagger(self):
        self.bigram_tagger = nltk.bigrams(self.words_pos_tags)
        g.logger.info("bigrams:::"+str(self.bigram_tagger))

    def gate_pos_tagging_using_jar(self):
        """
        Reference: https://gate.ac.uk/wiki/twitter-postagger.html
        L. Derczynski, A. Ritter, S. Clarke, and K. Bontcheva, 2013: "Twitter
        Part-of-Speech Tagging for All: Overcoming Sparse and Noisy Data". In:
        Proceedings of the International Conference on Recent Advances in Natural
        Language Processing.
        """
        '''subprocess.call(['java', '-jar', g.TWITTIE_JAR_PATH, g.TWITTIE_MODEL_PATH,
                         '/Users/maria/senti_feed_text_analysis_repo/senti_feed_text_analysis/senti_feed_text_analysis/data/twitie-tagger/sample.txt'])
                         #str(self.init_text)])'''
        pass

    def gate_experimental_pos_tagging_using_model(self):
        """
            Get pos tagging results using custom tagger with the model provided by gate twitter tagger.
            Reference: https://gate.ac.uk/wiki/twitter-postagger.html
            L. Derczynski, A. Ritter, S. Clarke, and K. Bontcheva, 2013: "Twitter
            Part-of-Speech Tagging for All: Overcoming Sparse and Noisy Data". In:
            Proceedings of the International Conference on Recent Advances in Natural
            Language Processing.
        """
        default_tagger = nltk.data.load(nltk.tag._POS_TAGGER)
        model = fh.train_model
        tagger = nltk.tag.UnigramTagger(model=model, backoff=default_tagger)
        self.gate_tagger = tagger.tag(self.tweet_words_in_a_single_list)
        g.logger.info('gate_tagger :::\t'+str(self.gate_tagger))

    def standford_tagger(self):
        words = self.standford_tagger.tag(self.tweet.words)
        print words
    #################################################### POS TAGGING ###################################################
    #################################################### STEMMING ######################################################
    def init_stemmers(self):
        self.porter_stemmer = nltk.PorterStemmer()
        self.lancaster_stemmer = nltk.LancasterStemmer()
        self.regex_stemmer = nltk.RegexpStemmer('^\w')
        self.iris_stemmer = nltk.ISRIStemmer()
        self.rspl_stemmer = nltk.RSLPStemmer()

    def stem_all(self, each, iris, lancaster, porter, regex, rspl):
        porter.append(self.porter_stemmer.stem(each))
        lancaster.append(self.lancaster_stemmer.stem(each))
        regex.append(self.regex_stemmer.stem(each))
        iris.append(self.iris_stemmer.stem(each))
        rspl.append(self.rspl_stemmer.stem(each))

    def apply_stemming(self):

        for word in self.tweet.words:
            if type(word) == list:
                for each in word:
                    self.stem_all(each, self.iris, self.lancaster, self.porter, self.regex, self.rspl)

            else:
                self.stem_all(word, self.iris, self.lancaster, self.porter, self.regex, self.rspl)

    #################################################### LEMMATIZING ###################################################
    def apply_lemmatizing(self):
        """
        should help to get better score...
        :return:
        """
        for word in self.tweet.words:
            if type(word) == list:
                for each in word:
                    self.lemmatized_words.append(self.lemmatizer.lemmatize(each))
            else:
                self.lemmatized_words.append(self.lemmatizer.lemmatize(word))
            #print "LEMMATIZED", self.lemmatized_words

    #################################################### HELPERS #######################################################
    #fixme: rethink about this...
    def get_tweet_words_in_a_single_list(self):
        tweet_words = []
        try:
            if type(self.tweet.words[0]) == list:
                for each in self.tweet.words:
                    tweet_words += each
            else:
                tweet_words = self.tweet.words
        except IndexError:
            tweet_words = self.tweet.words

        return tweet_words