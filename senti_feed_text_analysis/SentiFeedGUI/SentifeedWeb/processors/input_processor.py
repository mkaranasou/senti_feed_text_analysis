from senti_feed_text_analysis.controller.sentifeed_main import SentiFeed
from senti_feed_text_analysis.helpers import globals as g
from senti_feed_text_analysis.stream.stream_handler import StreamHandler

__author__ = 'maria'
__descr__ = 'This class is responsible to initiate the correct process depending on source input'


class InputHandler(object):
    def __init__(self, source_type, args, evaluation=False):
        self.source_type = source_type
        self.args = args
        self.evaluation = evaluation
        self.sentifeed = SentiFeed(g.DB_TYPE.MYSQL, g.DATA_TYPE.TWITTER_CORPUS, g.DATA_TYPE.SWN,
                      g.DATA_TYPE.STOP_WORDS, g.DATA_TYPE.EMOTICONS, g.TABLE.TwitterCorpusNoEmoticons, self.evaluation)
        self.sentifeed.get_things_rolling()

    def process_corpus_only(self, corpus):
        """
        Start processing corpus
        :param corpus: User Choice of corpus
        :return:
        """
        self.sentifeed.start_processing(corpus)
        self.sentifeed.evaluate_score()
        self.sentifeed.present_results()
        self.sentifeed.save_results()
        self.sentifeed.clean_up()

    def process_text_only(self, text):
        """
        For text processing, we do not need to evaluate against corpus and save to db
        :param text: User Input - string
        :return:
        """
        self.sentifeed.start_processing(text)
        #self.sentifeed.evaluate_score()
        #self.sentifeed.present_results()
        #self.sentifeed.save_results()
        self.sentifeed.clean_up()

    def process_streaming_data(self):
        """
        Different approach is being followed with streaming data.
        :return:
        """
        pass

    def init_processing(self, text, term=None, area=None, async=False):
        """
        Depending on source type, begin processing.
        :param text: text to be evaluated
        :param term: term for search in case of stream
        :param area: area for search in case of stream
        :param async: how to get stream working
        :return:
        """
        if self.source_type == g.SOURCE.Corpus:
            self.sentifeed.start_processing(self.sentifeed.get_data_list_item(g.DATA_TYPE.TWITTER_CORPUS))
        elif self.source_type == g.SOURCE.Text:
            self.process_text_only(text)
        elif self.source_type == g.SOURCE.Stream:
            stream_handler = StreamHandler()
            stream_handler.start_listening(term, area, async)
            self.process_streaming_data()
        else:
            raise NotImplementedError

