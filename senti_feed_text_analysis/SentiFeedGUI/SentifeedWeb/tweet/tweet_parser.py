import json
import traceback

__author__ = 'maria'

#For parsing Json objects from files or stream
class TweetParser():
    def __init__(self, json_text, filename):
        self.json_text = '{"created_at":"Tue Dec 31 21:41:56 +0000 2013","id":418134956162838528,"id_str":"418134956162838528","text":"@rolsen94 Congrats Ryan! Happy new year for you and for us! @NHLJets","source":"\u003ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003eTwitter for iPhone\u003c\/a\u003e","truncated":false,"in_reply_to_status_id":418131767271243778,"in_reply_to_status_id_str":"418131767271243778","in_reply_to_user_id":342448972,"in_reply_to_user_id_str":"342448972","in_reply_to_screen_name":"rolsen94","user":{"id":307089056,"id_str":"307089056","name":"Oly Backstrom","screen_name":"OlyBackstrom","location":"","url":null,"description":"Music fan, Winnipeg Jets fan. Believer in people of all abilities. Not a work account-but love my work, so may tweet occasionally about it  :)","protected":false,"followers_count":358,"friends_count":864,"listed_count":4,"created_at":"Sun May 29 00:50:12 +0000 2011","favourites_count":5760,"utc_offset":null,"time_zone":null,"geo_enabled":true,"verified":false,"statuses_count":4387,"lang":"en","contributors_enabled":false,"is_translator":false,"profile_background_color":"1A1B1F","profile_background_image_url":"http:\/\/abs.twimg.com\/images\/themes\/theme9\/bg.gif","profile_background_image_url_https":"https:\/\/abs.twimg.com\/images\/themes\/theme9\/bg.gif","profile_background_tile":false,"profile_image_url":"http:\/\/pbs.twimg.com\/profile_images\/378800000672208318\/2a55ebf9819f4a93905f3e8a7c691a8d_normal.jpeg","profile_image_url_https":"https:\/\/pbs.twimg.com\/profile_images\/378800000672208318\/2a55ebf9819f4a93905f3e8a7c691a8d_normal.jpeg","profile_banner_url":"https:\/\/pbs.twimg.com\/profile_banners\/307089056\/1383192102","profile_link_color":"2FC2EF","profile_sidebar_border_color":"181A1E","profile_sidebar_fill_color":"252429","profile_text_color":"666666","profile_use_background_image":true,"default_profile":false,"default_profile_image":false,"following":null,"follow_request_sent":null,"notifications":null},"geo":null,"coordinates":null,"place":null,"contributors":null,"retweet_count":0,"favorite_count":0,"entities":{"hashtags":[],"symbols":[],"urls":[],"user_mentions":[{"screen_name":"rolsen94","name":"Ryan Olsen","id":342448972,"id_str":"342448972","indices":[0,9]},{"screen_name":"NHLJets","name":"Winnipeg Jets","id":308556440,"id_str":"308556440","indices":[60,68]}]},"favorited":false,"retweeted":false,"filter_level":"medium","lang":"en"}'
        self.mydecoder = json.JSONDecoder()
        self.filename = filename
        self.tweet_json = {}

    def parse_json_from_file(self, filename):
        try:
            # pretty printing of json-formatted string
            #print json.dumps(decoded, sort_keys=True, indent=4)
            self.filename = filename
            with open(self.filename,'r') as f:
                for line in f:
                    if line!='\n':
                        record = self.mydecoder.decode(line)
                        try:
                            print "JSON parsing example: ", record['id_str']
                            print "Complex JSON parsing example: ", record['created_at'], "\\n text:", record['text']
                        except:
                            if record['limit']!= None:
                                print "limit:", record['limit']
                    else:
                        print "\\n line"
        except (ValueError, KeyError, TypeError):
            print "JSON format error"
            traceback.print_exc()

    def set_tweet_dictionary(self, tweet_json):
        self.tweet_json = tweet_json

    def update_tweet_table(self):
        pass

    def update_user_table(self):
        pass

    def update_entities_table(self):
        pass

#TODO sugirisma twn methodwn giati kanw multiple work
parser = TweetParser('../tweet/newyears.txt')
parser.parse_json_from_file('../tweet/newyears.txt')
