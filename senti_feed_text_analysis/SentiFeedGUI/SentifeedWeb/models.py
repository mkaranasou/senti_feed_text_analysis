from django.db import models
import datetime
import matplotlib.pyplot as mpl
from helpers import globals as g

# Create your models here.

class Tweet1(models.Model):
    TweetId = models.IntegerField()
    TweetText = models.CharField(max_length=150)
    TweetTextAfter = models.CharField(max_length=150)
    HandcodedNum = models.FloatField()
    HandcodedT = models.CharField(max_length=50)
    SumN = models.FloatField()
    CombinedN = models.FloatField()
    NVAR = models.FloatField()
    NVAROthersN= models.FloatField()
    NVARCombN = models.FloatField()
    CombinedT = models.CharField(max_length=50)
    Emoticons = models.CharField(max_length=150)
    EmoticonsScore = models.FloatField()

class Emotion(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=True)
    text = models.CharField(max_length=150)
    score_text = models.CharField(max_length=50)
    score = models.FloatField()

class SourceTable(object):
    def __init__(self, id, name):
        self.id = id
        self.name = name


class Tweet(object):
    id = None
    text = None
    create_date = None
    user = None
    entities = None
    lang = None
    processed_text = ''

    def __init__(self):
        self.links = []
        self.smileys_per_sentence = []
        self.sentences = []
        self.reference = []
        self.hash_list = []
        self.non_word_chars_removed = []
        self.words = []
        self.corrected_words = []
        self.negations = []
        self.uppercase_words_per_sentence = []
        self.stop_words_removed = []

class Text(object):
    pass

class CalculationMethod(object):
    id = None
    name = ''
    descr = ""
    type = None
    date_created = None
    accuracy = 0.0
    combined_accuracy = 0.0
    aspect_accuracy = 0.0
    aspect_score_accuracy = 0.0
    date_last_evaluated = datetime.datetime.now()
    def __init__(self, id, name, descr, type, date_created, accuracy, combined_accuracy, date_last_evaluated,
                 aspect_accuracy, aspect_score_accuracy):
        self.id = id
        self.name = name
        self.descr= descr
        self.type = type
        self.date_created = date_created
        self.accuracy = accuracy
        self.combined_accuracy = combined_accuracy
        self.aspect_accuracy = aspect_accuracy
        self.aspect_score_accuracy = aspect_score_accuracy
        self.date_last_evaluated = date_last_evaluated

    def __str__(self):
        return self._name

class Score(object):
    id = None
    twitter_corpus_id = None
    calculation_method_id = None
    score = 1.0
    combined_score = 1.0
    emoticons_score = 1.0
    aspects_list = []
    aspects_score_list = []
    date_updated = datetime.datetime.now()
    processed_tweet = ''
    def __init__(self, twitter_corpus_id, calculation_method_id):
        self.twitter_corpus_id = twitter_corpus_id
        self.calculation_method_id = calculation_method_id


class TwitterCorpus(object):
    id = None
    tweet_id = None
    tweet_text = ''
    tweet_text_after = ''
    emotion_text = ''
    emotion_num = 1.0
    score_id = None
    lang = ''
    emoticons = []
    date_created = None
    date_updated = None
    initial_aspect = ''
    aspect = ''

    def __init__(self, id, tweet_id, tweet_text, emotion_text, initial_aspect):
        self.id = id
        self.tweet_id = tweet_id
        self.tweet_text = tweet_text
        self.emotion_text = emotion_text
        self.initial_aspect = initial_aspect

class StatisticsHandler(object):
    stats_type = None
    chart_type = None
    stats_results = []

    def process(self):
        if self.stats_type == g.STATS_TYPE.TEXT_AND_SCORE:
            print self.stats_results[0]
            print self.stats_results[1]
            self.process_text_and_score()
        elif self.stats_type == g.STATS_TYPE.METHOD_AND_ACCURACY:
            self.process_method_and_accuracy()
        else:
            raise NotImplementedError

    def process_text_and_score(self):
        """
        test
        """
        if self.chart_type == g.CHART_TYPE.BAR:
            pass
        elif self.chart_type == g.CHART_TYPE.PIE:
            pass


    def process_method_and_accuracy(self):
        if self.chart_type == g.CHART_TYPE.BAR:
            fig = mpl.figure()
            ax1 = fig.add_subplot(4, 1, 1, axisbg='white')
            width = 0.2
            ax1.bar(self.stats_results[0][0], self.stats_results[1][1], width=width)
            ax1.set_xticks(self.stats_results[0][0])
            ax1.set_xticklabels(self.stats_results[0][1], rotation=90)
            ax1.set_title("General Accuracy")
            '''ax1 = fig.add_subplot(4, 1, 1, axisbg='white')
            ax1.plot(self.stats_results[0][0], self.stats_results[1][0], 'c', linewidth=5)
            ax1.set_title(str(g.STATS_TYPE.reverse_mapping[g.STATS_TYPE.METHOD_AND_ACCURACY]))'''

            #fig2 = pl.figure()
            ax2 = fig.add_subplot(4, 1, 2, axisbg='white')
            width = 0.5
            ax2.bar(self.stats_results[0][0], self.stats_results[1][1], width=width)
            ax2.set_xticks(self.stats_results[0][0])
            ax2.set_xticklabels(self.stats_results[0][1], rotation=90)
            ax2.set_title("Combined Accuracy")

            ax3 = fig.add_subplot(4, 1, 3, axisbg='white')
            width = 0.5
            ax3.bar(self.stats_results[0][0], self.stats_results[1][2], width=width)
            ax3.set_xticks(self.stats_results[0][0])
            ax3.set_xticklabels(self.stats_results[0][1], rotation=90)
            ax3.set_title("Aspect Accuracy")

            ax4 = fig.add_subplot(4, 1, 4, axisbg='white')
            width = 0.5
            ax4.bar(self.stats_results[0][0], self.stats_results[1][3], width=width)
            ax4.set_xticks(self.stats_results[0][0])
            ax4.set_xticklabels(self.stats_results[0][1], rotation=90)
            ax4.set_title("Aspect Score Accuracy")

            '''mpl.plot(self.stats_results[0][0], self.stats_results[1])

            mpl.xticks(self.stats_results[0][0], self.stats_results[0][1])'''

            mpl.savefig('..\\data\\stats\\stats_{0}.png'.format(g.DATESTAMP))
            #mpl.savefig('../data/stats/stats_{0}.png'.format(g.DATESTAMP))
            #mpl.show()


        elif self.chart_type == g.CHART_TYPE.PIE:
            pass




