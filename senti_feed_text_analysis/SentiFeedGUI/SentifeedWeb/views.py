from logging import log
from django.http import request
from django.shortcuts import render, HttpResponse
from django.shortcuts import render_to_response
from django.utils.datastructures import MultiValueDictKeyError
import requests
from controller.sentifeed_main import SentiFeed
import database as db
from models import Tweet1, Emotion, SourceTable
from helpers import globals as g

############### globals #####################
mysql_server_conn = db.MySQLDatabaseConnector()

# Create your views here.
sentifeed = SentiFeed(g.DB_TYPE.MYSQL, g.DATA_TYPE.TWITTER_CORPUS, g.DATA_TYPE.SWN,
                       g.DATA_TYPE.STOP_WORDS, g.DATA_TYPE.EMOTICONS, g.TABLE.TwitterCorpusNoEmoticons, True)
sentifeed.get_things_rolling()
s = requests.Session()

def home(request):
    #tweets = get_data()
    #all_tweets = Tweet.objects.all()
    header_response = {'title': 'IT\'S ON!!!!', 'h1': 'SentifeedWeb Application', 'h2':'Network Oriented Systems Thesis 2014',}
                       #'tweets': tweets}

    return render_to_response('index.html', header_response)


def get_data():
    '''rows = mysql_server_conn.execute_query(g.select_from_sentifeedresults_for_view)
    results = []
    for row in rows:
        tweet = Tweet()
        tweet.TweetText = row[0]  # initial tweet text
        tweet.TweetTextAfter = row[1]  #
        tweet.HandcodedT = row[3]
        tweet.CombinedT = row[9]
        tweet.SumN = row[4]
        results.append(tweet)
    #mysql_server_conn.close_conn()
    return results'''
    pass

def evaluate_emotion(text_to_be_evaluated):
    #result_list = start_processing(text_to_be_evaluated, 'text')
    return None

def evaluate(request):
    tweets = get_data()
    emotion = Emotion()
    results = evaluate_emotion(request.GET['to_be_evaluated'])
    emotion.score_text = results[0]
    tweet = fill_tweet_obj_results(results[1])
    header_response = {'title': 'IT\'S ON!!!!', 'h1': 'SentifeedWeb Application',
                       'h2':'Network Oriented Systems Thesis 2014',
                       'h3' : 'YAYYY!!!',
                       'tweets': tweets, 'emotion': emotion.score_text,
                       'to_be_evaluated':request.GET['to_be_evaluated'],
                       'scores': tweet}
    return render_to_response('evaluate.html', header_response)


def fill_tweet_obj_results(text_analyser):
    tweet_object = Tweet1()
    tweet_object.TweetText = text_analyser.init_text
    tweet_object.TweetTextAfter = text_analyser.final_tweet
    #tweet_object.HandcodedT = text_analyser.handcoded_t
    #tweet_object.HandcodedNum = text_analyser.handcoded_n
    tweet_object.SumN = text_analyser.tweet_score
    tweet_object.CombinedN = text_analyser.combined_score
    tweet_object.NVAR = text_analyser.nvar_score
    tweet_object.NVAROthersN = text_analyser.non_nvar_score
    tweet_object.NVARCombN = text_analyser.nvar_combi_score
    tweet_object.Emoticons = text_analyser.smileys_per_sentence
    tweet_object.EmoticonsScore = text_analyser.emoticons_score
    return tweet_object

    return tweet_object


def streaming(request):
    header_response = {
        'title' : "Twitter Streaming Application",
        'h1': 'SentifeedWeb Application',
        'h2':'Network Oriented Systems Thesis 2014',
    }
    return render_to_response('streaming.html', header_response)


def get_tables(mysql_server_conn):
     tables = mysql_server_conn.execute_query(g.select_all_from_source)
     table_list = []
     for table in tables:
        table_list.append(SourceTable(table[0], table[1]))
     return  table_list


def corpus(request):
    if request.GET.get('corpus_table')!= None and request.is_ajax():
       corpus_results(request)
       return None
    else:
        corpus_tables = get_tables(mysql_server_conn)
    header_response = {
        'title' : "Twitter Corpus Application",
        'h1': 'SentifeedWeb Application',
        'h2':'Network Oriented Systems Thesis 2014',
        'corpus_tables' : corpus_tables,
    }

    return render_to_response('corpus.html', header_response)


def try_it(request):
    result_text = None
    if request.method == 'GET':
        result_text = sentifeed.start_processing(str(request.GET.get('to_evaluate')))
    header_response = {
        'title' : "Try Sentifeed Application",
        'h1': 'SentifeedWeb Application',
        'h2':'Network Oriented Systems Thesis 2014',
        'result_text' : result_text,
    }
    return render_to_response('try_it.html', header_response)


def about(request):
    header_response = {
        'title': "About",
        'h1': 'SentifeedWeb Application',
        'h2':'Network Oriented Systems Thesis 2014',
    }
    return render_to_response('about.html', header_response)


def corpus_results(request):
        if request.method == 'POST':
            corpus_table = request.POST.get('corpus_table')
            top = request.POST.get('top')
            corpus_data = mysql_server_conn.execute_query(g.select_from_corpus
                                                          .format(table=corpus_table, limit=top))
        elif request.method == 'GET':
            print 'in get'
            corpus_table = request.GET.get('corpus_table')
            top = request.GET.get('top')
            corpus_data = mysql_server_conn.execute_query(g.select_from_corpus
                                                          .format(table=corpus_table, limit=top))
        header_response = {
            'title' : "Twitter Corpus Application",
            'h1': 'SentifeedWeb Application',
            'h2':'Network Oriented Systems Thesis 2014',
            'corpus_data'   : corpus_data,
            }

        return render_to_response('corpus_results.html', header_response)

def streaming():
    headers = {'connection': 'keep-alive', 'content-type': 'application/json', 'x-powered-by': 'Express', 'transfer-encoding': 'chunked'}
    req = requests.Request("GET",'http://127.0.0.1:8000/static/data/big.txt',
                           headers=headers).prepare()

    resp = s.send(req, stream=True)

    for line in resp.iter_lines():
        if line:
            yield line


def read_stream():

    for line in streaming():
        print line