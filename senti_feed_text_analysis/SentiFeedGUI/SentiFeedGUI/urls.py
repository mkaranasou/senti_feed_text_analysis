from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'SentifeedWeb.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^/((get|post)?(?P<lowercase_string>[a-z]+))', 'SentifeedWeb.views.evaluate', name='evaluate'),
    #url(r'^$', 'SentifeedWeb.views.evaluate', name='evaluate'),
    url(r'^evaluate/', 'SentifeedWeb.views.evaluate', name='evaluate'),
    url(r'^streaming/', 'SentifeedWeb.views.streaming', name='streaming'),
    url(r'^corpus/', 'SentifeedWeb.views.corpus', name='corpus'),
    url(r'^corpus/corpus_results/', 'SentifeedWeb.views.corpus_results', name='corpus_results'),
    url(r'^try_it/', 'SentifeedWeb.views.try_it', name='try_it'),
    url(r'^about/', 'SentifeedWeb.views.about', name='about'),
    url(r'^admin/', include(admin.site.urls)),

)
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )